﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ImageCompressionUI.TaskControllers;
using ImageCompressionUI.TaskControllers.JPEGTask;
using ImageCompressionUI.TaskControllers.WaveletTask;

namespace ImageCompressionUI
{
	public partial class MainForm : Form
	{
		public readonly MainFormState State;
		private ITaskController taskController;

		public MainForm()
		{
			InitializeComponent();
			State = new MainFormState(this);

			PSNRTaskPage.Tag = new PSNRTaskController();
			GrayscaleTaskPage.Tag = new GrayscaleTaskController();
			ConvertTaskPage.Tag = new Rgb2YCbCrTaskController();
			LBGTaskPage.Tag = new LBGTaskController();
			LBGPositionTaskPage.Tag = new LBGPTaskController();
			JPEGTaskPage.Tag = new JPEGTaskController();
			WaveletTaskPage.Tag = new WaveletTaskController();

			taskController = new PSNRTaskController();
			taskController.BindEvents(this);
		}

		private void TaskPanel_Selected(object sender, TabControlEventArgs e)
		{
			taskController.UnbindEvents(this);
			taskController = (ITaskController) e.TabPage.Tag;
			taskController.BindEvents(this);
			taskController.Initialize();
		}

		private void OpenLeftImageButton_Click(object sender, EventArgs args)
		{
			LoadImage("Open left image", State.LoadLeftImage);
		}

		private void OpenRightImageButton_Click(object sender, EventArgs e)
		{
			LoadImage("Open right image", State.LoadRightImage);
		}

		private void SaveLeftImageButton_Click(object sender, EventArgs e)
		{
			SaveImage("Save left image", LeftImageBox.Image);
		}

		private void SaveRightImageButton_Click(object sender, EventArgs args)
		{
			SaveImage("Save right image", RightImageBox.Image);
		}

		private void LoadImage(string title, Action<string> loadImage)
		{
			var openDialog = new OpenFileDialog
			{
				Title = title,
				Filter = "Image (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg"
			};
			if (openDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					loadImage(openDialog.FileName);
				}
				catch (Exception e)
				{
					ShowError(e);
				}
			}
		}

		private void SaveImage(string title, Image image)
		{
			var saveFileDialog = new SaveFileDialog
			{
				AddExtension = true,
				DefaultExt = ".png",
				Filter = "Image (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg",
				Title = title
			};
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					image.Save(saveFileDialog.FileName);
				}
				catch (Exception e)
				{
					ShowError(e);
				}
			}
		}

		private void ShowError(Exception e)
		{
			var uiException = e as UIException;
			var title = uiException == null ? "Unexpected error" : uiException.Title;
			MessageBox.Show(e.Message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}