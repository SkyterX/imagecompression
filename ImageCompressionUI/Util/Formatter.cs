﻿using System.Text;

namespace ImageCompressionUI.Util
{
	public static class Formatter
	{
		public static string FormatMemory(long memory)
		{
			var kb = (memory / 1024) % 1024;
			var mb = (memory / (1024 * 1024)) % 1024;
			var gb = (memory / (1024 * 1024 * 1024));
			var sb = new StringBuilder();
			if (gb > 0)
				sb.AppendFormat("{0}Gb ", gb);
			if (sb.Length != 0 || mb > 0)
				sb.AppendFormat("{0}Mb ", mb);
			if (sb.Length != 0 || kb > 0)
				sb.AppendFormat("{0}Kb", kb);
			return sb.ToString();
		} 
	}
}