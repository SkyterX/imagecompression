﻿using System;

namespace ImageCompressionUI.Util
{
	public static class EventExtensions
	{
		public static void Raise<T>(this EventHandler<T> eventHandler, object sender, T eventData) 
			where T : EventArgs
		{
			var handler = eventHandler;
			if (handler != null) 
				handler(sender, eventData);
		}

		public static void Raise(this EventHandler eventHandler, object sender)
		{
			var handler = eventHandler;
			if (handler != null)
				handler(sender, EventArgs.Empty);
		}
	}
}