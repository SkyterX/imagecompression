﻿using System.Drawing;

namespace ImageCompressionUI.Util
{
	public static class BitmapExtensions
	{
		public static void Fill(this Bitmap bitmap, Color color)
		{
			using (var graphics = Graphics.FromImage(bitmap))
			using (var brush = new SolidBrush(color))
			{
				graphics.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
			}
		}
	}
}