﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ImageCompressionUI.Util
{
	public static class EnumerableExtensions
	{
		/// <summary>
		/// Wraps this object instance into an Array
		/// consisting of a single item.
		/// </summary>
		/// <typeparam name="T"> Type of the wrapped object.</typeparam>
		/// <param name="item"> The object to wrap.</param>
		/// <returns>
		/// An Array consisting of a single item.
		/// </returns>
		public static T[] Yield<T>(this T item)
		{
			return new[] {item};
		}

		public static int IndexOfFirst<T>(this IEnumerable<T> source, Predicate<T> predicate)
		{
			if (source == null) throw new ArgumentNullException("source");
			int elementIndex = 0;
			foreach (var element in source)
			{
				if (predicate(element))
					return elementIndex;
				elementIndex++;
			}
			return -1;
		}

		public static int IndexOfLast<T>(this IEnumerable<T> source, Predicate<T> predicate)
		{
			if (source == null) throw new ArgumentNullException("source");
			int matchingElementIndex = -1;
			int elementIndex = 0;
			foreach (var element in source)
			{
				if (predicate(element))
					matchingElementIndex = elementIndex;
				elementIndex++;
			}
			return matchingElementIndex;
		}

		public static TVal ArgMax<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> selector)
			where TComp : IComparable<TComp>
		{
			return source.ArgBest(selector, (best, val) => best.CompareTo(val) < 0);
		}

		public static TVal ArgMin<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> selector)
			where TComp : IComparable<TComp>
		{
			return source.ArgBest(selector, (best, val) => best.CompareTo(val) > 0);
		}

		private static TVal ArgBest<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> converter,
			Func<TComp, TComp, bool> accept)
		{
			if (source == null)
				throw new ArgumentNullException("source");
			bool first = true;
			TVal best = default(TVal);
			TComp bestValue = default(TComp);
			foreach (var element in source)
			{
				var value = converter(element);
				if (first || accept(bestValue, value))
				{
					best = element;
					bestValue = value;
					first = false;
				}
			}
			if (first)
				throw new InvalidOperationException("Sequence contains no elements");
			return best;
		}

		public static int IndexOfMax<TVal>(this IEnumerable<TVal> source) where TVal : IComparable<TVal>
		{
			return source.IndexOfBest(val => val, (best, val) => best.CompareTo(val) < 0);
		}

		public static int IndexOfMin<TVal>(this IEnumerable<TVal> source) where TVal : IComparable<TVal>
		{
			return source.IndexOfBest(val => val, (best, val) => best.CompareTo(val) > 0);
		}

		public static int IndexOfMax<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> selector)
			where TComp : IComparable<TComp>
		{
			return source.IndexOfBest(selector, (best, val) => best.CompareTo(val) < 0);
		}

		public static int IndexOfMin<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> selector)
			where TComp : IComparable<TComp>
		{
			return source.IndexOfBest(selector, (best, val) => best.CompareTo(val) > 0);
		}

		private static int IndexOfBest<TVal, TComp>(this IEnumerable<TVal> source, Converter<TVal, TComp> converter,
			Func<TComp, TComp, bool> accept)
		{
			if (source == null)
				throw new ArgumentNullException("source");
			int bestIndex = -1;
			int currentIndex = -1;
			TComp bestValue = default(TComp);
			foreach (var element in source)
			{
				currentIndex++;
				var value = converter(element);
				if (bestIndex == -1 || accept(bestValue, value))
				{
					bestValue = value;
					bestIndex = currentIndex;
				}
			}
			if (currentIndex == -1)
				throw new InvalidOperationException("Sequence contains no elements");
			return bestIndex;
		}

		public static IEnumerable<T> ExceptNull<T>(this IEnumerable<T> source) where T : class
		{
			if (source == null) throw new ArgumentNullException("source");
			return source.Where(element => element != null);
		}

		public static T[] Shuffle<T>(this IEnumerable<T> source, int randomSeed = 0)
		{
			return Shuffle(source, new Random(randomSeed));
		}

		public static T[] Shuffle<T>(this IEnumerable<T> source, Random random)
		{
			if (source == null) throw new ArgumentNullException("source");
			T[] shuffled = source.ToArray();
			for (int i = 0; i < shuffled.Length - 1; i++)
			{
				int ind = random.Next(i, shuffled.Length);
				T t = shuffled[i];
				shuffled[i] = shuffled[ind];
				shuffled[ind] = t;
			}
			return shuffled;
		}

		public static T Random<T>(this IList<T> source, int randomSeed)
		{
			return source.Random(new Random(randomSeed));
		}

		public static T Random<T>(this IList<T> source, Random random = null)
		{
			random = random ?? new Random();
			return source[random.Next(source.Count)];
		}

		public static IEnumerable<T> Concat<T>(this IEnumerable<T> source, T element)
		{
			if (source == null) throw new ArgumentNullException("source");
			foreach (var item in source)
			{
				yield return item;
			}
			yield return element;
		}

		public static IEnumerable<T> Concat<T>(this IEnumerable<T> first, IEnumerable<T> second, IEnumerable<T> third,
			params IEnumerable<T>[] other)
		{
			if (first == null) throw new ArgumentNullException("first");
			if (second == null) throw new ArgumentNullException("second");
			if (third == null) throw new ArgumentNullException("third");
			return other.Aggregate(first.Concat(second).Concat(third), Enumerable.Concat);
		}

		public static HashSet<T> ToSet<T>(this IEnumerable<T> source)
		{
			return new HashSet<T>(source);
		}

		public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> source) where T : IComparable<T>
		{
			return source.OrderBy(x => x);
		}

		public static IOrderedEnumerable<T> OrderByDescending<T>(this IEnumerable<T> source) where T : IComparable<T>
		{
			return source.OrderByDescending(x => x);
		}

		public static T[] ConvertThis<T>(this T[] array, Func<T, T> converter)
		{
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = converter(array[i]);
			}
			return array;
		}

		public static IList<T> ConvertThis<T>(this IList<T> list, Func<T, T> converter)
		{
			for (int i = 0; i < list.Count; i++)
			{
				list[i] = converter(list[i]);
			}
			return list;
		}

		public static T[] Fill<T>(this T[] array, T value)
		{
			for (var i = 0; i < array.Length; i++)
			{
				array[i] = value;
			}
			return array;
		}

		public static T[] Fill<T>(this T[] array, Func<int, T> constructor)
		{
			for (var i = 0; i < array.Length; i++)
			{
				array[i] = constructor(i);
			}
			return array;
		}

		public static IList<T> Fill<T>(this IList<T> list, T value)
		{
			for (var i = 0; i < list.Count; i++)
			{
				list[i] = value;
			}
			return list;
		}

		public static IList<T> Fill<T>(this IList<T> list, Func<int, T> constructor)
		{
			for (var i = 0; i < list.Count; i++)
			{
				list[i] = constructor(i);
			}
			return list;
		}

		public static TSource MaxOrDefault<TSource>(this IEnumerable<TSource> source,
			TSource defaulValue = default(TSource)) where TSource : IComparable<TSource>
		{
			return source.DefaultIfEmpty(defaulValue).Max();
		}

		public static TSource MinOrDefault<TSource>(this IEnumerable<TSource> source,
			TSource defaulValue = default(TSource)) where TSource : IComparable<TSource>
		{
			return source.DefaultIfEmpty(defaulValue).Min();
		}

		public static TResult MaxOrDefault<TSource, TResult>(this IEnumerable<TSource> source,
			Func<TSource, TResult> selector, TResult defaulValue = default(TResult)) where TResult : IComparable<TResult>
		{
			return source.Select(selector).DefaultIfEmpty(defaulValue).Max();
		}

		public static TResult MinOrDefault<TSource, TResult>(this IEnumerable<TSource> source,
			Func<TSource, TResult> selector, TResult defaulValue = default(TResult)) where TResult : IComparable<TResult>
		{
			return source.Select(selector).DefaultIfEmpty(defaulValue).Min();
		}
	}
}