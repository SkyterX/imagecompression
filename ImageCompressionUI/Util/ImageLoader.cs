﻿using System;
using System.Drawing;

namespace ImageCompressionUI.Util
{
	public static class ImageLoader
	{
		public static Bitmap LoadImage(string fileName)
		{
			try
			{
				return new Bitmap(fileName);
			}
			catch (Exception)
			{
				throw new UIException("Open Error", "Can't open image file : " + fileName);
			}
		}
	}
}