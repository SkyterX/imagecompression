﻿using System;
using System.Drawing;
using ImageCompressionUI.ImageProcessing;
using ImageCompressionUI.Util;

namespace ImageCompressionUI
{
	public class MainFormState
	{
		private readonly Bitmap defaultImage;
		private readonly RgbImage defaultRgbImage;

		private readonly MainForm mainForm;

		public MainFormState(MainForm mainForm)
		{
			this.mainForm = mainForm;
			defaultImage = new Bitmap(512, 512);
			leftBitmap = new Bitmap(512, 512);
			rightBitmap = new Bitmap(512, 512);
			defaultImage.Fill(Color.White);
			defaultRgbImage = new RgbImage(defaultImage);
			SetLeftBitmap(defaultRgbImage);
			SetRightBitmap(defaultRgbImage);
			leftImage = defaultRgbImage;
			rightImage = defaultRgbImage;
		}

		private RgbImage leftImage;
		private RgbImage rightImage;
		private readonly Bitmap leftBitmap;
		private readonly Bitmap rightBitmap;
		private double psnr;

		public double PSNR
		{
			get { return psnr; }
			set
			{
				psnr = value;
				mainForm.PSNRValueLabel.Text = double.IsPositiveInfinity(psnr) ? "Infinity" : psnr.ToString("0.00");
			}
		}

		public RgbImage LeftImage
		{
			get { return leftImage; }
			set
			{
				leftImage = value ?? defaultRgbImage;
				LeftImageUpdated.Raise(this);
			}
		}

		public RgbImage RightImage
		{
			get { return rightImage; }
			set
			{
				rightImage = value ?? defaultRgbImage;
				RightImageUpdated.Raise(this);
			}
		}

		public void SetLeftBitmap(RgbImage image)
		{
			image.ToBitmap(leftBitmap);
			mainForm.LeftImageBox.Image = leftBitmap;
		}

		public void SetRightBitmap(RgbImage image)
		{
			image.ToBitmap(rightBitmap);
			mainForm.RightImageBox.Image = rightBitmap;
		}

		public void ClearRightBitmap()
		{
			SetRightBitmap(defaultRgbImage);
			UpdatePSNR(LeftImage, defaultRgbImage);
		}

		public event EventHandler LeftImageUpdated = delegate { };

		public event EventHandler RightImageUpdated = delegate { };

		public void LoadLeftImage(string fileName)
		{
			var image = ImageLoader.LoadImage(fileName);
			LeftImage = new RgbImage(image);
		}

		public void LoadRightImage(string fileName)
		{
			var image = ImageLoader.LoadImage(fileName);
			RightImage = new RgbImage(image);
		}

		public void UpdatePSNR(RgbImage a, RgbImage b)
		{
			PSNR = ImageStats.PSNR(a, b);
		}
	}
}