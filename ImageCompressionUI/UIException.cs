﻿using System;

namespace ImageCompressionUI
{
	public class UIException : Exception
	{
		public readonly string Title;

		public UIException(string title, string message)
			: base(message)
		{
			Title = title;
		}
	}
}