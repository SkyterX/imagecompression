﻿namespace ImageCompressionUI
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LeftImageBox = new System.Windows.Forms.PictureBox();
			this.RightImageBox = new System.Windows.Forms.PictureBox();
			this.OpenLeftImageButton = new System.Windows.Forms.Button();
			this.PSNRLabel = new System.Windows.Forms.Label();
			this.PSNRValueLabel = new System.Windows.Forms.Label();
			this.YTrackBar = new System.Windows.Forms.TrackBar();
			this.CbTrackBar = new System.Windows.Forms.TrackBar();
			this.CrTrackBar = new System.Windows.Forms.TrackBar();
			this.YTrackLabel = new System.Windows.Forms.Label();
			this.CbTrackLabel = new System.Windows.Forms.Label();
			this.CrTrackLabel = new System.Windows.Forms.Label();
			this.Y0TickLabel = new System.Windows.Forms.Label();
			this.Y4TickLabel = new System.Windows.Forms.Label();
			this.Y8TickLabel = new System.Windows.Forms.Label();
			this.U8TickLabel = new System.Windows.Forms.Label();
			this.U4TickLabel = new System.Windows.Forms.Label();
			this.U0TickLabel = new System.Windows.Forms.Label();
			this.V8TickLabel = new System.Windows.Forms.Label();
			this.V4TickLabel = new System.Windows.Forms.Label();
			this.V0TickLabel = new System.Windows.Forms.Label();
			this.TaskPanel = new System.Windows.Forms.TabControl();
			this.PSNRTaskPage = new System.Windows.Forms.TabPage();
			this.ConvertTaskPage = new System.Windows.Forms.TabPage();
			this.GrayscaleTaskPage = new System.Windows.Forms.TabPage();
			this.GrayscaleRightImageLabel = new System.Windows.Forms.Label();
			this.GrayscaleLeftImageLabel = new System.Windows.Forms.Label();
			this.GrayscaleCCIRRadio = new System.Windows.Forms.RadioButton();
			this.GrayscaleCMPRadio = new System.Windows.Forms.RadioButton();
			this.GrayscaleEQRadio = new System.Windows.Forms.RadioButton();
			this.LBGTaskPage = new System.Windows.Forms.TabPage();
			this.LBGApplyButton = new System.Windows.Forms.Button();
			this.LBGCountValueLabel = new System.Windows.Forms.Label();
			this.LBGCountTrackBar = new System.Windows.Forms.TrackBar();
			this.LBGCountLabel = new System.Windows.Forms.Label();
			this.LBGPositionTaskPage = new System.Windows.Forms.TabPage();
			this.LBGPRelationLabel = new System.Windows.Forms.Label();
			this.LBGPCountValue = new System.Windows.Forms.NumericUpDown();
			this.LBGPApplyButton = new System.Windows.Forms.Button();
			this.LBGPRelationValueLabel = new System.Windows.Forms.Label();
			this.LBGPRatioTrackBar = new System.Windows.Forms.TrackBar();
			this.LBGPCountLabel = new System.Windows.Forms.Label();
			this.JPEGTaskPage = new System.Windows.Forms.TabPage();
			this.JPEGQuantizationParamDValue = new System.Windows.Forms.NumericUpDown();
			this.JPEGQuantizationParamDLabel = new System.Windows.Forms.Label();
			this.JPEGQuantizationParamCValue = new System.Windows.Forms.NumericUpDown();
			this.JPEGQuantizationParamCLabel = new System.Windows.Forms.Label();
			this.JPEGOpenButton = new System.Windows.Forms.Button();
			this.JPEGQuantizationParamBValue = new System.Windows.Forms.NumericUpDown();
			this.JPEGQuantizationParamBLabel = new System.Windows.Forms.Label();
			this.JPEGQuantizationParamAValue = new System.Windows.Forms.NumericUpDown();
			this.JPEGQuantizationParamALabel = new System.Windows.Forms.Label();
			this.JPEGSizeLabel = new System.Windows.Forms.Label();
			this.JPEGSaveButton = new System.Windows.Forms.Button();
			this.JPEGQuantizationBox = new System.Windows.Forms.ComboBox();
			this.JPEGQuantizationLabel = new System.Windows.Forms.Label();
			this.JPEGApplyButton = new System.Windows.Forms.Button();
			this.JPEGDownsamplingBox = new System.Windows.Forms.ComboBox();
			this.JPEGDownsamplingLabel = new System.Windows.Forms.Label();
			this.WaveletTaskPage = new System.Windows.Forms.TabPage();
			this.WaveletDepthValue = new System.Windows.Forms.NumericUpDown();
			this.WaveletDepthLabel = new System.Windows.Forms.Label();
			this.WaveletTypeBox = new System.Windows.Forms.ComboBox();
			this.WaveletTypeLabel = new System.Windows.Forms.Label();
			this.SaveRightImageButton = new System.Windows.Forms.Button();
			this.SaveLeftImageButton = new System.Windows.Forms.Button();
			this.OpenRightImageButton = new System.Windows.Forms.Button();
			this.WaveletThresholdValue = new System.Windows.Forms.NumericUpDown();
			this.WaveletThresholdLabel = new System.Windows.Forms.Label();
			this.WaveletCompressRatioValue = new System.Windows.Forms.Label();
			this.WaveletCompressRatioLabel = new System.Windows.Forms.Label();
			this.WaveletCompressedSizeLabel = new System.Windows.Forms.Label();
			this.WaveletCompressedSizeValue = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.LeftImageBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RightImageBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.YTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CbTrackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CrTrackBar)).BeginInit();
			this.TaskPanel.SuspendLayout();
			this.ConvertTaskPage.SuspendLayout();
			this.GrayscaleTaskPage.SuspendLayout();
			this.LBGTaskPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.LBGCountTrackBar)).BeginInit();
			this.LBGPositionTaskPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.LBGPCountValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LBGPRatioTrackBar)).BeginInit();
			this.JPEGTaskPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamDValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamCValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamBValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamAValue)).BeginInit();
			this.WaveletTaskPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.WaveletDepthValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.WaveletThresholdValue)).BeginInit();
			this.SuspendLayout();
			// 
			// LeftImageBox
			// 
			this.LeftImageBox.BackColor = System.Drawing.SystemColors.Window;
			this.LeftImageBox.Location = new System.Drawing.Point(12, 12);
			this.LeftImageBox.Name = "LeftImageBox";
			this.LeftImageBox.Size = new System.Drawing.Size(512, 512);
			this.LeftImageBox.TabIndex = 0;
			this.LeftImageBox.TabStop = false;
			// 
			// RightImageBox
			// 
			this.RightImageBox.BackColor = System.Drawing.SystemColors.Window;
			this.RightImageBox.Location = new System.Drawing.Point(545, 12);
			this.RightImageBox.Name = "RightImageBox";
			this.RightImageBox.Size = new System.Drawing.Size(512, 512);
			this.RightImageBox.TabIndex = 1;
			this.RightImageBox.TabStop = false;
			// 
			// OpenLeftImageButton
			// 
			this.OpenLeftImageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.OpenLeftImageButton.Location = new System.Drawing.Point(12, 543);
			this.OpenLeftImageButton.Name = "OpenLeftImageButton";
			this.OpenLeftImageButton.Size = new System.Drawing.Size(138, 28);
			this.OpenLeftImageButton.TabIndex = 2;
			this.OpenLeftImageButton.Text = "Open Left Image";
			this.OpenLeftImageButton.UseVisualStyleBackColor = true;
			this.OpenLeftImageButton.Click += new System.EventHandler(this.OpenLeftImageButton_Click);
			// 
			// PSNRLabel
			// 
			this.PSNRLabel.AutoSize = true;
			this.PSNRLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PSNRLabel.Location = new System.Drawing.Point(12, 624);
			this.PSNRLabel.Name = "PSNRLabel";
			this.PSNRLabel.Size = new System.Drawing.Size(70, 20);
			this.PSNRLabel.TabIndex = 3;
			this.PSNRLabel.Text = "PSNR : ";
			this.PSNRLabel.UseWaitCursor = true;
			// 
			// PSNRValueLabel
			// 
			this.PSNRValueLabel.AutoSize = true;
			this.PSNRValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.PSNRValueLabel.Location = new System.Drawing.Point(88, 624);
			this.PSNRValueLabel.Name = "PSNRValueLabel";
			this.PSNRValueLabel.Size = new System.Drawing.Size(18, 20);
			this.PSNRValueLabel.TabIndex = 4;
			this.PSNRValueLabel.Text = "0";
			this.PSNRValueLabel.UseWaitCursor = true;
			// 
			// YTrackBar
			// 
			this.YTrackBar.Location = new System.Drawing.Point(7, 36);
			this.YTrackBar.Maximum = 8;
			this.YTrackBar.Name = "YTrackBar";
			this.YTrackBar.Size = new System.Drawing.Size(128, 45);
			this.YTrackBar.TabIndex = 5;
			this.YTrackBar.Value = 8;
			// 
			// CbTrackBar
			// 
			this.CbTrackBar.Location = new System.Drawing.Point(187, 36);
			this.CbTrackBar.Maximum = 8;
			this.CbTrackBar.Name = "CbTrackBar";
			this.CbTrackBar.Size = new System.Drawing.Size(128, 45);
			this.CbTrackBar.TabIndex = 6;
			this.CbTrackBar.Value = 8;
			// 
			// CrTrackBar
			// 
			this.CrTrackBar.Location = new System.Drawing.Point(363, 36);
			this.CrTrackBar.Maximum = 8;
			this.CrTrackBar.Name = "CrTrackBar";
			this.CrTrackBar.Size = new System.Drawing.Size(128, 45);
			this.CrTrackBar.TabIndex = 7;
			this.CrTrackBar.Value = 8;
			// 
			// YTrackLabel
			// 
			this.YTrackLabel.AutoSize = true;
			this.YTrackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
			this.YTrackLabel.Location = new System.Drawing.Point(25, 12);
			this.YTrackLabel.Name = "YTrackLabel";
			this.YTrackLabel.Size = new System.Drawing.Size(92, 20);
			this.YTrackLabel.TabIndex = 9;
			this.YTrackLabel.Text = "Bytes for Y";
			// 
			// CbTrackLabel
			// 
			this.CbTrackLabel.AutoSize = true;
			this.CbTrackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
			this.CbTrackLabel.Location = new System.Drawing.Point(204, 12);
			this.CbTrackLabel.Name = "CbTrackLabel";
			this.CbTrackLabel.Size = new System.Drawing.Size(103, 20);
			this.CbTrackLabel.TabIndex = 10;
			this.CbTrackLabel.Text = "Bytes for Cb";
			// 
			// CrTrackLabel
			// 
			this.CrTrackLabel.AutoSize = true;
			this.CrTrackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
			this.CrTrackLabel.Location = new System.Drawing.Point(382, 12);
			this.CrTrackLabel.Name = "CrTrackLabel";
			this.CrTrackLabel.Size = new System.Drawing.Size(100, 20);
			this.CrTrackLabel.TabIndex = 11;
			this.CrTrackLabel.Text = "Bytes for Cr";
			// 
			// Y0TickLabel
			// 
			this.Y0TickLabel.AutoSize = true;
			this.Y0TickLabel.Location = new System.Drawing.Point(14, 63);
			this.Y0TickLabel.Name = "Y0TickLabel";
			this.Y0TickLabel.Size = new System.Drawing.Size(13, 13);
			this.Y0TickLabel.TabIndex = 12;
			this.Y0TickLabel.Text = "0";
			// 
			// Y4TickLabel
			// 
			this.Y4TickLabel.AutoSize = true;
			this.Y4TickLabel.Location = new System.Drawing.Point(65, 63);
			this.Y4TickLabel.Name = "Y4TickLabel";
			this.Y4TickLabel.Size = new System.Drawing.Size(13, 13);
			this.Y4TickLabel.TabIndex = 13;
			this.Y4TickLabel.Text = "4";
			// 
			// Y8TickLabel
			// 
			this.Y8TickLabel.AutoSize = true;
			this.Y8TickLabel.Location = new System.Drawing.Point(115, 63);
			this.Y8TickLabel.Name = "Y8TickLabel";
			this.Y8TickLabel.Size = new System.Drawing.Size(13, 13);
			this.Y8TickLabel.TabIndex = 14;
			this.Y8TickLabel.Text = "8";
			// 
			// U8TickLabel
			// 
			this.U8TickLabel.AutoSize = true;
			this.U8TickLabel.Location = new System.Drawing.Point(295, 63);
			this.U8TickLabel.Name = "U8TickLabel";
			this.U8TickLabel.Size = new System.Drawing.Size(13, 13);
			this.U8TickLabel.TabIndex = 17;
			this.U8TickLabel.Text = "8";
			// 
			// U4TickLabel
			// 
			this.U4TickLabel.AutoSize = true;
			this.U4TickLabel.Location = new System.Drawing.Point(245, 63);
			this.U4TickLabel.Name = "U4TickLabel";
			this.U4TickLabel.Size = new System.Drawing.Size(13, 13);
			this.U4TickLabel.TabIndex = 16;
			this.U4TickLabel.Text = "4";
			// 
			// U0TickLabel
			// 
			this.U0TickLabel.AutoSize = true;
			this.U0TickLabel.Location = new System.Drawing.Point(194, 63);
			this.U0TickLabel.Name = "U0TickLabel";
			this.U0TickLabel.Size = new System.Drawing.Size(13, 13);
			this.U0TickLabel.TabIndex = 15;
			this.U0TickLabel.Text = "0";
			// 
			// V8TickLabel
			// 
			this.V8TickLabel.AutoSize = true;
			this.V8TickLabel.Location = new System.Drawing.Point(471, 63);
			this.V8TickLabel.Name = "V8TickLabel";
			this.V8TickLabel.Size = new System.Drawing.Size(13, 13);
			this.V8TickLabel.TabIndex = 20;
			this.V8TickLabel.Text = "8";
			// 
			// V4TickLabel
			// 
			this.V4TickLabel.AutoSize = true;
			this.V4TickLabel.Location = new System.Drawing.Point(421, 63);
			this.V4TickLabel.Name = "V4TickLabel";
			this.V4TickLabel.Size = new System.Drawing.Size(13, 13);
			this.V4TickLabel.TabIndex = 19;
			this.V4TickLabel.Text = "4";
			// 
			// V0TickLabel
			// 
			this.V0TickLabel.AutoSize = true;
			this.V0TickLabel.Location = new System.Drawing.Point(370, 63);
			this.V0TickLabel.Name = "V0TickLabel";
			this.V0TickLabel.Size = new System.Drawing.Size(13, 13);
			this.V0TickLabel.TabIndex = 18;
			this.V0TickLabel.Text = "0";
			// 
			// TaskPanel
			// 
			this.TaskPanel.Controls.Add(this.PSNRTaskPage);
			this.TaskPanel.Controls.Add(this.ConvertTaskPage);
			this.TaskPanel.Controls.Add(this.GrayscaleTaskPage);
			this.TaskPanel.Controls.Add(this.LBGTaskPage);
			this.TaskPanel.Controls.Add(this.LBGPositionTaskPage);
			this.TaskPanel.Controls.Add(this.JPEGTaskPage);
			this.TaskPanel.Controls.Add(this.WaveletTaskPage);
			this.TaskPanel.Location = new System.Drawing.Point(545, 543);
			this.TaskPanel.Multiline = true;
			this.TaskPanel.Name = "TaskPanel";
			this.TaskPanel.SelectedIndex = 0;
			this.TaskPanel.Size = new System.Drawing.Size(512, 152);
			this.TaskPanel.TabIndex = 21;
			this.TaskPanel.Selected += new System.Windows.Forms.TabControlEventHandler(this.TaskPanel_Selected);
			// 
			// PSNRTaskPage
			// 
			this.PSNRTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.PSNRTaskPage.Location = new System.Drawing.Point(4, 22);
			this.PSNRTaskPage.Name = "PSNRTaskPage";
			this.PSNRTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.PSNRTaskPage.Size = new System.Drawing.Size(504, 126);
			this.PSNRTaskPage.TabIndex = 1;
			this.PSNRTaskPage.Text = "PSNR";
			// 
			// ConvertTaskPage
			// 
			this.ConvertTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.ConvertTaskPage.Controls.Add(this.V8TickLabel);
			this.ConvertTaskPage.Controls.Add(this.YTrackLabel);
			this.ConvertTaskPage.Controls.Add(this.V4TickLabel);
			this.ConvertTaskPage.Controls.Add(this.V0TickLabel);
			this.ConvertTaskPage.Controls.Add(this.U8TickLabel);
			this.ConvertTaskPage.Controls.Add(this.CrTrackBar);
			this.ConvertTaskPage.Controls.Add(this.U4TickLabel);
			this.ConvertTaskPage.Controls.Add(this.CbTrackLabel);
			this.ConvertTaskPage.Controls.Add(this.U0TickLabel);
			this.ConvertTaskPage.Controls.Add(this.CrTrackLabel);
			this.ConvertTaskPage.Controls.Add(this.Y8TickLabel);
			this.ConvertTaskPage.Controls.Add(this.Y0TickLabel);
			this.ConvertTaskPage.Controls.Add(this.Y4TickLabel);
			this.ConvertTaskPage.Controls.Add(this.YTrackBar);
			this.ConvertTaskPage.Controls.Add(this.CbTrackBar);
			this.ConvertTaskPage.Location = new System.Drawing.Point(4, 22);
			this.ConvertTaskPage.Name = "ConvertTaskPage";
			this.ConvertTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.ConvertTaskPage.Size = new System.Drawing.Size(504, 126);
			this.ConvertTaskPage.TabIndex = 0;
			this.ConvertTaskPage.Text = "RGB to YCbCr";
			// 
			// GrayscaleTaskPage
			// 
			this.GrayscaleTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.GrayscaleTaskPage.Controls.Add(this.GrayscaleRightImageLabel);
			this.GrayscaleTaskPage.Controls.Add(this.GrayscaleLeftImageLabel);
			this.GrayscaleTaskPage.Controls.Add(this.GrayscaleCCIRRadio);
			this.GrayscaleTaskPage.Controls.Add(this.GrayscaleCMPRadio);
			this.GrayscaleTaskPage.Controls.Add(this.GrayscaleEQRadio);
			this.GrayscaleTaskPage.Location = new System.Drawing.Point(4, 22);
			this.GrayscaleTaskPage.Name = "GrayscaleTaskPage";
			this.GrayscaleTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.GrayscaleTaskPage.Size = new System.Drawing.Size(504, 126);
			this.GrayscaleTaskPage.TabIndex = 2;
			this.GrayscaleTaskPage.Text = "Grayscale";
			// 
			// GrayscaleRightImageLabel
			// 
			this.GrayscaleRightImageLabel.AutoSize = true;
			this.GrayscaleRightImageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.GrayscaleRightImageLabel.Location = new System.Drawing.Point(394, 43);
			this.GrayscaleRightImageLabel.Name = "GrayscaleRightImageLabel";
			this.GrayscaleRightImageLabel.Size = new System.Drawing.Size(104, 20);
			this.GrayscaleRightImageLabel.TabIndex = 4;
			this.GrayscaleRightImageLabel.Text = "Right - CCIR";
			// 
			// GrayscaleLeftImageLabel
			// 
			this.GrayscaleLeftImageLabel.AutoSize = true;
			this.GrayscaleLeftImageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.GrayscaleLeftImageLabel.Location = new System.Drawing.Point(7, 43);
			this.GrayscaleLeftImageLabel.Name = "GrayscaleLeftImageLabel";
			this.GrayscaleLeftImageLabel.Size = new System.Drawing.Size(147, 20);
			this.GrayscaleLeftImageLabel.TabIndex = 3;
			this.GrayscaleLeftImageLabel.Text = "Left - equal weight";
			// 
			// GrayscaleCCIRRadio
			// 
			this.GrayscaleCCIRRadio.AutoSize = true;
			this.GrayscaleCCIRRadio.Checked = true;
			this.GrayscaleCCIRRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.GrayscaleCCIRRadio.Location = new System.Drawing.Point(361, 12);
			this.GrayscaleCCIRRadio.Name = "GrayscaleCCIRRadio";
			this.GrayscaleCCIRRadio.Size = new System.Drawing.Size(137, 24);
			this.GrayscaleCCIRRadio.TabIndex = 2;
			this.GrayscaleCCIRRadio.TabStop = true;
			this.GrayscaleCCIRRadio.Text = "CCIR standard";
			this.GrayscaleCCIRRadio.UseVisualStyleBackColor = true;
			// 
			// GrayscaleCMPRadio
			// 
			this.GrayscaleCMPRadio.AutoSize = true;
			this.GrayscaleCMPRadio.Checked = true;
			this.GrayscaleCMPRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.GrayscaleCMPRadio.Location = new System.Drawing.Point(203, 12);
			this.GrayscaleCMPRadio.Name = "GrayscaleCMPRadio";
			this.GrayscaleCMPRadio.Size = new System.Drawing.Size(95, 24);
			this.GrayscaleCMPRadio.TabIndex = 1;
			this.GrayscaleCMPRadio.TabStop = true;
			this.GrayscaleCMPRadio.Text = "Compare";
			this.GrayscaleCMPRadio.UseVisualStyleBackColor = true;
			// 
			// GrayscaleEQRadio
			// 
			this.GrayscaleEQRadio.AutoSize = true;
			this.GrayscaleEQRadio.Checked = true;
			this.GrayscaleEQRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.GrayscaleEQRadio.Location = new System.Drawing.Point(6, 12);
			this.GrayscaleEQRadio.Name = "GrayscaleEQRadio";
			this.GrayscaleEQRadio.Size = new System.Drawing.Size(122, 24);
			this.GrayscaleEQRadio.TabIndex = 0;
			this.GrayscaleEQRadio.TabStop = true;
			this.GrayscaleEQRadio.Text = "Equal weight";
			this.GrayscaleEQRadio.UseVisualStyleBackColor = true;
			// 
			// LBGTaskPage
			// 
			this.LBGTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.LBGTaskPage.Controls.Add(this.LBGApplyButton);
			this.LBGTaskPage.Controls.Add(this.LBGCountValueLabel);
			this.LBGTaskPage.Controls.Add(this.LBGCountTrackBar);
			this.LBGTaskPage.Controls.Add(this.LBGCountLabel);
			this.LBGTaskPage.Location = new System.Drawing.Point(4, 22);
			this.LBGTaskPage.Name = "LBGTaskPage";
			this.LBGTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.LBGTaskPage.Size = new System.Drawing.Size(504, 126);
			this.LBGTaskPage.TabIndex = 3;
			this.LBGTaskPage.Text = "LBG colors";
			// 
			// LBGApplyButton
			// 
			this.LBGApplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGApplyButton.Location = new System.Drawing.Point(317, 8);
			this.LBGApplyButton.Name = "LBGApplyButton";
			this.LBGApplyButton.Size = new System.Drawing.Size(164, 28);
			this.LBGApplyButton.TabIndex = 24;
			this.LBGApplyButton.Text = "Apply selected colors";
			this.LBGApplyButton.UseVisualStyleBackColor = true;
			// 
			// LBGCountValueLabel
			// 
			this.LBGCountValueLabel.AutoSize = true;
			this.LBGCountValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGCountValueLabel.Location = new System.Drawing.Point(206, 12);
			this.LBGCountValueLabel.Name = "LBGCountValueLabel";
			this.LBGCountValueLabel.Size = new System.Drawing.Size(27, 20);
			this.LBGCountValueLabel.TabIndex = 2;
			this.LBGCountValueLabel.Text = "32";
			// 
			// LBGCountTrackBar
			// 
			this.LBGCountTrackBar.Location = new System.Drawing.Point(5, 45);
			this.LBGCountTrackBar.Maximum = 256;
			this.LBGCountTrackBar.Minimum = 1;
			this.LBGCountTrackBar.Name = "LBGCountTrackBar";
			this.LBGCountTrackBar.Size = new System.Drawing.Size(495, 45);
			this.LBGCountTrackBar.TabIndex = 1;
			this.LBGCountTrackBar.Value = 32;
			// 
			// LBGCountLabel
			// 
			this.LBGCountLabel.AutoSize = true;
			this.LBGCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGCountLabel.Location = new System.Drawing.Point(6, 12);
			this.LBGCountLabel.Name = "LBGCountLabel";
			this.LBGCountLabel.Size = new System.Drawing.Size(194, 20);
			this.LBGCountLabel.TabIndex = 0;
			this.LBGCountLabel.Text = "Number of colors used : ";
			// 
			// LBGPositionTaskPage
			// 
			this.LBGPositionTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.LBGPositionTaskPage.Controls.Add(this.LBGPRelationLabel);
			this.LBGPositionTaskPage.Controls.Add(this.LBGPCountValue);
			this.LBGPositionTaskPage.Controls.Add(this.LBGPApplyButton);
			this.LBGPositionTaskPage.Controls.Add(this.LBGPRelationValueLabel);
			this.LBGPositionTaskPage.Controls.Add(this.LBGPRatioTrackBar);
			this.LBGPositionTaskPage.Controls.Add(this.LBGPCountLabel);
			this.LBGPositionTaskPage.Location = new System.Drawing.Point(4, 22);
			this.LBGPositionTaskPage.Name = "LBGPositionTaskPage";
			this.LBGPositionTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.LBGPositionTaskPage.Size = new System.Drawing.Size(504, 126);
			this.LBGPositionTaskPage.TabIndex = 4;
			this.LBGPositionTaskPage.Text = "LBG colors + position";
			// 
			// LBGPRelationLabel
			// 
			this.LBGPRelationLabel.AutoSize = true;
			this.LBGPRelationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGPRelationLabel.Location = new System.Drawing.Point(143, 12);
			this.LBGPRelationLabel.Name = "LBGPRelationLabel";
			this.LBGPRelationLabel.Size = new System.Drawing.Size(192, 20);
			this.LBGPRelationLabel.TabIndex = 30;
			this.LBGPRelationLabel.Text = "Color Vs Position ratio : ";
			// 
			// LBGPCountValue
			// 
			this.LBGPCountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGPCountValue.Location = new System.Drawing.Point(85, 12);
			this.LBGPCountValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.LBGPCountValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.LBGPCountValue.Name = "LBGPCountValue";
			this.LBGPCountValue.Size = new System.Drawing.Size(52, 23);
			this.LBGPCountValue.TabIndex = 29;
			this.LBGPCountValue.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
			// 
			// LBGPApplyButton
			// 
			this.LBGPApplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGPApplyButton.Location = new System.Drawing.Point(395, 8);
			this.LBGPApplyButton.Name = "LBGPApplyButton";
			this.LBGPApplyButton.Size = new System.Drawing.Size(86, 28);
			this.LBGPApplyButton.TabIndex = 28;
			this.LBGPApplyButton.Text = "Apply";
			this.LBGPApplyButton.UseVisualStyleBackColor = true;
			// 
			// LBGPRelationValueLabel
			// 
			this.LBGPRelationValueLabel.AutoSize = true;
			this.LBGPRelationValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGPRelationValueLabel.Location = new System.Drawing.Point(341, 12);
			this.LBGPRelationValueLabel.Name = "LBGPRelationValueLabel";
			this.LBGPRelationValueLabel.Size = new System.Drawing.Size(40, 20);
			this.LBGPRelationValueLabel.TabIndex = 27;
			this.LBGPRelationValueLabel.Text = "0.00";
			// 
			// LBGPRatioTrackBar
			// 
			this.LBGPRatioTrackBar.Location = new System.Drawing.Point(5, 45);
			this.LBGPRatioTrackBar.Maximum = 100;
			this.LBGPRatioTrackBar.Minimum = -100;
			this.LBGPRatioTrackBar.Name = "LBGPRatioTrackBar";
			this.LBGPRatioTrackBar.Size = new System.Drawing.Size(495, 45);
			this.LBGPRatioTrackBar.TabIndex = 26;
			// 
			// LBGPCountLabel
			// 
			this.LBGPCountLabel.AutoSize = true;
			this.LBGPCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.LBGPCountLabel.Location = new System.Drawing.Point(6, 12);
			this.LBGPCountLabel.Name = "LBGPCountLabel";
			this.LBGPCountLabel.Size = new System.Drawing.Size(73, 20);
			this.LBGPCountLabel.TabIndex = 25;
			this.LBGPCountLabel.Text = "Colors : ";
			// 
			// JPEGTaskPage
			// 
			this.JPEGTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamDValue);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamDLabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamCValue);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamCLabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGOpenButton);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamBValue);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamBLabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamAValue);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationParamALabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGSizeLabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGSaveButton);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationBox);
			this.JPEGTaskPage.Controls.Add(this.JPEGQuantizationLabel);
			this.JPEGTaskPage.Controls.Add(this.JPEGApplyButton);
			this.JPEGTaskPage.Controls.Add(this.JPEGDownsamplingBox);
			this.JPEGTaskPage.Controls.Add(this.JPEGDownsamplingLabel);
			this.JPEGTaskPage.Location = new System.Drawing.Point(4, 22);
			this.JPEGTaskPage.Name = "JPEGTaskPage";
			this.JPEGTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.JPEGTaskPage.Size = new System.Drawing.Size(504, 126);
			this.JPEGTaskPage.TabIndex = 5;
			this.JPEGTaskPage.Text = "JPEG";
			// 
			// JPEGQuantizationParamDValue
			// 
			this.JPEGQuantizationParamDValue.DecimalPlaces = 1;
			this.JPEGQuantizationParamDValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamDValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.JPEGQuantizationParamDValue.Location = new System.Drawing.Point(422, 89);
			this.JPEGQuantizationParamDValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.JPEGQuantizationParamDValue.Name = "JPEGQuantizationParamDValue";
			this.JPEGQuantizationParamDValue.Size = new System.Drawing.Size(52, 23);
			this.JPEGQuantizationParamDValue.TabIndex = 42;
			this.JPEGQuantizationParamDValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.JPEGQuantizationParamDValue.Visible = false;
			// 
			// JPEGQuantizationParamDLabel
			// 
			this.JPEGQuantizationParamDLabel.AutoSize = true;
			this.JPEGQuantizationParamDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamDLabel.Location = new System.Drawing.Point(381, 90);
			this.JPEGQuantizationParamDLabel.Name = "JPEGQuantizationParamDLabel";
			this.JPEGQuantizationParamDLabel.Size = new System.Drawing.Size(36, 20);
			this.JPEGQuantizationParamDLabel.TabIndex = 41;
			this.JPEGQuantizationParamDLabel.Text = "B : ";
			this.JPEGQuantizationParamDLabel.Visible = false;
			// 
			// JPEGQuantizationParamCValue
			// 
			this.JPEGQuantizationParamCValue.DecimalPlaces = 1;
			this.JPEGQuantizationParamCValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamCValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.JPEGQuantizationParamCValue.Location = new System.Drawing.Point(302, 89);
			this.JPEGQuantizationParamCValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.JPEGQuantizationParamCValue.Name = "JPEGQuantizationParamCValue";
			this.JPEGQuantizationParamCValue.Size = new System.Drawing.Size(52, 23);
			this.JPEGQuantizationParamCValue.TabIndex = 40;
			this.JPEGQuantizationParamCValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.JPEGQuantizationParamCValue.Visible = false;
			// 
			// JPEGQuantizationParamCLabel
			// 
			this.JPEGQuantizationParamCLabel.AutoSize = true;
			this.JPEGQuantizationParamCLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamCLabel.Location = new System.Drawing.Point(261, 90);
			this.JPEGQuantizationParamCLabel.Name = "JPEGQuantizationParamCLabel";
			this.JPEGQuantizationParamCLabel.Size = new System.Drawing.Size(35, 20);
			this.JPEGQuantizationParamCLabel.TabIndex = 39;
			this.JPEGQuantizationParamCLabel.Text = "A : ";
			this.JPEGQuantizationParamCLabel.Visible = false;
			// 
			// JPEGOpenButton
			// 
			this.JPEGOpenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGOpenButton.Location = new System.Drawing.Point(133, 85);
			this.JPEGOpenButton.Name = "JPEGOpenButton";
			this.JPEGOpenButton.Size = new System.Drawing.Size(103, 28);
			this.JPEGOpenButton.TabIndex = 38;
			this.JPEGOpenButton.Text = "Open";
			this.JPEGOpenButton.UseVisualStyleBackColor = true;
			// 
			// JPEGQuantizationParamBValue
			// 
			this.JPEGQuantizationParamBValue.DecimalPlaces = 1;
			this.JPEGQuantizationParamBValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamBValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.JPEGQuantizationParamBValue.Location = new System.Drawing.Point(422, 54);
			this.JPEGQuantizationParamBValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.JPEGQuantizationParamBValue.Name = "JPEGQuantizationParamBValue";
			this.JPEGQuantizationParamBValue.Size = new System.Drawing.Size(52, 23);
			this.JPEGQuantizationParamBValue.TabIndex = 37;
			this.JPEGQuantizationParamBValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.JPEGQuantizationParamBValue.Visible = false;
			// 
			// JPEGQuantizationParamBLabel
			// 
			this.JPEGQuantizationParamBLabel.AutoSize = true;
			this.JPEGQuantizationParamBLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamBLabel.Location = new System.Drawing.Point(381, 55);
			this.JPEGQuantizationParamBLabel.Name = "JPEGQuantizationParamBLabel";
			this.JPEGQuantizationParamBLabel.Size = new System.Drawing.Size(36, 20);
			this.JPEGQuantizationParamBLabel.TabIndex = 36;
			this.JPEGQuantizationParamBLabel.Text = "B : ";
			this.JPEGQuantizationParamBLabel.Visible = false;
			// 
			// JPEGQuantizationParamAValue
			// 
			this.JPEGQuantizationParamAValue.DecimalPlaces = 1;
			this.JPEGQuantizationParamAValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamAValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.JPEGQuantizationParamAValue.Location = new System.Drawing.Point(302, 54);
			this.JPEGQuantizationParamAValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.JPEGQuantizationParamAValue.Name = "JPEGQuantizationParamAValue";
			this.JPEGQuantizationParamAValue.Size = new System.Drawing.Size(52, 23);
			this.JPEGQuantizationParamAValue.TabIndex = 35;
			this.JPEGQuantizationParamAValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.JPEGQuantizationParamAValue.Visible = false;
			// 
			// JPEGQuantizationParamALabel
			// 
			this.JPEGQuantizationParamALabel.AutoSize = true;
			this.JPEGQuantizationParamALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationParamALabel.Location = new System.Drawing.Point(261, 55);
			this.JPEGQuantizationParamALabel.Name = "JPEGQuantizationParamALabel";
			this.JPEGQuantizationParamALabel.Size = new System.Drawing.Size(35, 20);
			this.JPEGQuantizationParamALabel.TabIndex = 34;
			this.JPEGQuantizationParamALabel.Text = "A : ";
			this.JPEGQuantizationParamALabel.Visible = false;
			// 
			// JPEGSizeLabel
			// 
			this.JPEGSizeLabel.AutoSize = true;
			this.JPEGSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGSizeLabel.Location = new System.Drawing.Point(129, 54);
			this.JPEGSizeLabel.Name = "JPEGSizeLabel";
			this.JPEGSizeLabel.Size = new System.Drawing.Size(43, 20);
			this.JPEGSizeLabel.TabIndex = 33;
			this.JPEGSizeLabel.Text = "0 Kb";
			// 
			// JPEGSaveButton
			// 
			this.JPEGSaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGSaveButton.Location = new System.Drawing.Point(10, 85);
			this.JPEGSaveButton.Name = "JPEGSaveButton";
			this.JPEGSaveButton.Size = new System.Drawing.Size(105, 28);
			this.JPEGSaveButton.TabIndex = 32;
			this.JPEGSaveButton.Text = "Save";
			this.JPEGSaveButton.UseVisualStyleBackColor = true;
			// 
			// JPEGQuantizationBox
			// 
			this.JPEGQuantizationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.JPEGQuantizationBox.FormattingEnabled = true;
			this.JPEGQuantizationBox.Items.AddRange(new object[] {
            "4:4:4",
            "4:2:2 (2h1v)",
            "4:2:2 (1h2v)",
            "4:1:1 (2h2v)"});
			this.JPEGQuantizationBox.Location = new System.Drawing.Point(354, 14);
			this.JPEGQuantizationBox.Name = "JPEGQuantizationBox";
			this.JPEGQuantizationBox.Size = new System.Drawing.Size(144, 21);
			this.JPEGQuantizationBox.TabIndex = 31;
			// 
			// JPEGQuantizationLabel
			// 
			this.JPEGQuantizationLabel.AutoSize = true;
			this.JPEGQuantizationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGQuantizationLabel.Location = new System.Drawing.Point(242, 14);
			this.JPEGQuantizationLabel.Name = "JPEGQuantizationLabel";
			this.JPEGQuantizationLabel.Size = new System.Drawing.Size(118, 20);
			this.JPEGQuantizationLabel.TabIndex = 30;
			this.JPEGQuantizationLabel.Text = "Quantization : ";
			// 
			// JPEGApplyButton
			// 
			this.JPEGApplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGApplyButton.Location = new System.Drawing.Point(10, 51);
			this.JPEGApplyButton.Name = "JPEGApplyButton";
			this.JPEGApplyButton.Size = new System.Drawing.Size(105, 28);
			this.JPEGApplyButton.TabIndex = 29;
			this.JPEGApplyButton.Text = "Apply";
			this.JPEGApplyButton.UseVisualStyleBackColor = true;
			// 
			// JPEGDownsamplingBox
			// 
			this.JPEGDownsamplingBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.JPEGDownsamplingBox.FormattingEnabled = true;
			this.JPEGDownsamplingBox.Items.AddRange(new object[] {
            "4:4:4",
            "4:2:2 (2h1v)",
            "4:2:2 (1h2v)",
            "4:1:1 (2h2v)"});
			this.JPEGDownsamplingBox.Location = new System.Drawing.Point(146, 14);
			this.JPEGDownsamplingBox.Name = "JPEGDownsamplingBox";
			this.JPEGDownsamplingBox.Size = new System.Drawing.Size(90, 21);
			this.JPEGDownsamplingBox.TabIndex = 2;
			// 
			// JPEGDownsamplingLabel
			// 
			this.JPEGDownsamplingLabel.AutoSize = true;
			this.JPEGDownsamplingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.JPEGDownsamplingLabel.Location = new System.Drawing.Point(6, 14);
			this.JPEGDownsamplingLabel.Name = "JPEGDownsamplingLabel";
			this.JPEGDownsamplingLabel.Size = new System.Drawing.Size(134, 20);
			this.JPEGDownsamplingLabel.TabIndex = 1;
			this.JPEGDownsamplingLabel.Text = "Downsampling : ";
			// 
			// WaveletTaskPage
			// 
			this.WaveletTaskPage.BackColor = System.Drawing.SystemColors.Control;
			this.WaveletTaskPage.Controls.Add(this.WaveletCompressedSizeLabel);
			this.WaveletTaskPage.Controls.Add(this.WaveletCompressedSizeValue);
			this.WaveletTaskPage.Controls.Add(this.WaveletCompressRatioLabel);
			this.WaveletTaskPage.Controls.Add(this.WaveletCompressRatioValue);
			this.WaveletTaskPage.Controls.Add(this.WaveletThresholdValue);
			this.WaveletTaskPage.Controls.Add(this.WaveletThresholdLabel);
			this.WaveletTaskPage.Controls.Add(this.WaveletDepthValue);
			this.WaveletTaskPage.Controls.Add(this.WaveletDepthLabel);
			this.WaveletTaskPage.Controls.Add(this.WaveletTypeBox);
			this.WaveletTaskPage.Controls.Add(this.WaveletTypeLabel);
			this.WaveletTaskPage.Location = new System.Drawing.Point(4, 22);
			this.WaveletTaskPage.Name = "WaveletTaskPage";
			this.WaveletTaskPage.Padding = new System.Windows.Forms.Padding(3);
			this.WaveletTaskPage.Size = new System.Drawing.Size(504, 126);
			this.WaveletTaskPage.TabIndex = 6;
			this.WaveletTaskPage.Text = "Wavelet";
			// 
			// WaveletDepthValue
			// 
			this.WaveletDepthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletDepthValue.Location = new System.Drawing.Point(396, 14);
			this.WaveletDepthValue.Maximum = new decimal(new int[] {
            65335,
            0,
            0,
            0});
			this.WaveletDepthValue.Name = "WaveletDepthValue";
			this.WaveletDepthValue.Size = new System.Drawing.Size(52, 23);
			this.WaveletDepthValue.TabIndex = 37;
			this.WaveletDepthValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// WaveletDepthLabel
			// 
			this.WaveletDepthLabel.AutoSize = true;
			this.WaveletDepthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletDepthLabel.Location = new System.Drawing.Point(292, 14);
			this.WaveletDepthLabel.Name = "WaveletDepthLabel";
			this.WaveletDepthLabel.Size = new System.Drawing.Size(69, 20);
			this.WaveletDepthLabel.TabIndex = 36;
			this.WaveletDepthLabel.Text = "Depth : ";
			// 
			// WaveletTypeBox
			// 
			this.WaveletTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.WaveletTypeBox.FormattingEnabled = true;
			this.WaveletTypeBox.Items.AddRange(new object[] {
            "4:4:4",
            "4:2:2 (2h1v)",
            "4:2:2 (1h2v)",
            "4:1:1 (2h2v)"});
			this.WaveletTypeBox.Location = new System.Drawing.Point(146, 14);
			this.WaveletTypeBox.Name = "WaveletTypeBox";
			this.WaveletTypeBox.Size = new System.Drawing.Size(90, 21);
			this.WaveletTypeBox.TabIndex = 4;
			// 
			// WaveletTypeLabel
			// 
			this.WaveletTypeLabel.AutoSize = true;
			this.WaveletTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletTypeLabel.Location = new System.Drawing.Point(6, 14);
			this.WaveletTypeLabel.Name = "WaveletTypeLabel";
			this.WaveletTypeLabel.Size = new System.Drawing.Size(120, 20);
			this.WaveletTypeLabel.TabIndex = 3;
			this.WaveletTypeLabel.Text = "Wavelet type : ";
			// 
			// SaveRightImageButton
			// 
			this.SaveRightImageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SaveRightImageButton.Location = new System.Drawing.Point(386, 577);
			this.SaveRightImageButton.Name = "SaveRightImageButton";
			this.SaveRightImageButton.Size = new System.Drawing.Size(137, 28);
			this.SaveRightImageButton.TabIndex = 22;
			this.SaveRightImageButton.Text = "Save Right Image";
			this.SaveRightImageButton.UseVisualStyleBackColor = true;
			this.SaveRightImageButton.Click += new System.EventHandler(this.SaveRightImageButton_Click);
			// 
			// SaveLeftImageButton
			// 
			this.SaveLeftImageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SaveLeftImageButton.Location = new System.Drawing.Point(12, 577);
			this.SaveLeftImageButton.Name = "SaveLeftImageButton";
			this.SaveLeftImageButton.Size = new System.Drawing.Size(137, 28);
			this.SaveLeftImageButton.TabIndex = 24;
			this.SaveLeftImageButton.Text = "Save Left Image";
			this.SaveLeftImageButton.UseVisualStyleBackColor = true;
			this.SaveLeftImageButton.Click += new System.EventHandler(this.SaveLeftImageButton_Click);
			// 
			// OpenRightImageButton
			// 
			this.OpenRightImageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.OpenRightImageButton.Location = new System.Drawing.Point(386, 543);
			this.OpenRightImageButton.Name = "OpenRightImageButton";
			this.OpenRightImageButton.Size = new System.Drawing.Size(138, 28);
			this.OpenRightImageButton.TabIndex = 23;
			this.OpenRightImageButton.Text = "Open Right Image";
			this.OpenRightImageButton.UseVisualStyleBackColor = true;
			this.OpenRightImageButton.Click += new System.EventHandler(this.OpenRightImageButton_Click);
			// 
			// WaveletThresholdValue
			// 
			this.WaveletThresholdValue.DecimalPlaces = 2;
			this.WaveletThresholdValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletThresholdValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.WaveletThresholdValue.Location = new System.Drawing.Point(396, 51);
			this.WaveletThresholdValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.WaveletThresholdValue.Name = "WaveletThresholdValue";
			this.WaveletThresholdValue.Size = new System.Drawing.Size(52, 23);
			this.WaveletThresholdValue.TabIndex = 39;
			// 
			// WaveletThresholdLabel
			// 
			this.WaveletThresholdLabel.AutoSize = true;
			this.WaveletThresholdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletThresholdLabel.Location = new System.Drawing.Point(292, 52);
			this.WaveletThresholdLabel.Name = "WaveletThresholdLabel";
			this.WaveletThresholdLabel.Size = new System.Drawing.Size(98, 20);
			this.WaveletThresholdLabel.TabIndex = 38;
			this.WaveletThresholdLabel.Text = "Threshold : ";
			// 
			// WaveletCompressRatioValue
			// 
			this.WaveletCompressRatioValue.AutoSize = true;
			this.WaveletCompressRatioValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletCompressRatioValue.Location = new System.Drawing.Point(167, 51);
			this.WaveletCompressRatioValue.Name = "WaveletCompressRatioValue";
			this.WaveletCompressRatioValue.Size = new System.Drawing.Size(18, 20);
			this.WaveletCompressRatioValue.TabIndex = 40;
			this.WaveletCompressRatioValue.Text = "1";
			// 
			// WaveletCompressRatioLabel
			// 
			this.WaveletCompressRatioLabel.AutoSize = true;
			this.WaveletCompressRatioLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletCompressRatioLabel.Location = new System.Drawing.Point(6, 52);
			this.WaveletCompressRatioLabel.Name = "WaveletCompressRatioLabel";
			this.WaveletCompressRatioLabel.Size = new System.Drawing.Size(139, 20);
			this.WaveletCompressRatioLabel.TabIndex = 41;
			this.WaveletCompressRatioLabel.Text = "Compress ratio : ";
			// 
			// WaveletCompressedSizeLabel
			// 
			this.WaveletCompressedSizeLabel.AutoSize = true;
			this.WaveletCompressedSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletCompressedSizeLabel.Location = new System.Drawing.Point(6, 86);
			this.WaveletCompressedSizeLabel.Name = "WaveletCompressedSizeLabel";
			this.WaveletCompressedSizeLabel.Size = new System.Drawing.Size(155, 20);
			this.WaveletCompressedSizeLabel.TabIndex = 43;
			this.WaveletCompressedSizeLabel.Text = "Compressed size : ";
			// 
			// WaveletCompressedSizeValue
			// 
			this.WaveletCompressedSizeValue.AutoSize = true;
			this.WaveletCompressedSizeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WaveletCompressedSizeValue.Location = new System.Drawing.Point(167, 86);
			this.WaveletCompressedSizeValue.Name = "WaveletCompressedSizeValue";
			this.WaveletCompressedSizeValue.Size = new System.Drawing.Size(18, 20);
			this.WaveletCompressedSizeValue.TabIndex = 42;
			this.WaveletCompressedSizeValue.Text = "1";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1078, 707);
			this.Controls.Add(this.SaveLeftImageButton);
			this.Controls.Add(this.OpenRightImageButton);
			this.Controls.Add(this.SaveRightImageButton);
			this.Controls.Add(this.TaskPanel);
			this.Controls.Add(this.PSNRValueLabel);
			this.Controls.Add(this.PSNRLabel);
			this.Controls.Add(this.OpenLeftImageButton);
			this.Controls.Add(this.RightImageBox);
			this.Controls.Add(this.LeftImageBox);
			this.Name = "MainForm";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.LeftImageBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RightImageBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.YTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CbTrackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CrTrackBar)).EndInit();
			this.TaskPanel.ResumeLayout(false);
			this.ConvertTaskPage.ResumeLayout(false);
			this.ConvertTaskPage.PerformLayout();
			this.GrayscaleTaskPage.ResumeLayout(false);
			this.GrayscaleTaskPage.PerformLayout();
			this.LBGTaskPage.ResumeLayout(false);
			this.LBGTaskPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.LBGCountTrackBar)).EndInit();
			this.LBGPositionTaskPage.ResumeLayout(false);
			this.LBGPositionTaskPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.LBGPCountValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LBGPRatioTrackBar)).EndInit();
			this.JPEGTaskPage.ResumeLayout(false);
			this.JPEGTaskPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamDValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamCValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamBValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.JPEGQuantizationParamAValue)).EndInit();
			this.WaveletTaskPage.ResumeLayout(false);
			this.WaveletTaskPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.WaveletDepthValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.WaveletThresholdValue)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public System.Windows.Forms.PictureBox LeftImageBox;
		public System.Windows.Forms.PictureBox RightImageBox;
		public System.Windows.Forms.Button OpenLeftImageButton;
		public System.Windows.Forms.Button SaveLeftImageButton;
		public System.Windows.Forms.Button OpenRightImageButton;
		public System.Windows.Forms.Button SaveRightImageButton;
		private System.Windows.Forms.Label PSNRLabel;
		public System.Windows.Forms.Label PSNRValueLabel;
		public System.Windows.Forms.TrackBar YTrackBar;
		public System.Windows.Forms.TrackBar CbTrackBar;
		public System.Windows.Forms.TrackBar CrTrackBar;
		private System.Windows.Forms.Label YTrackLabel;
		private System.Windows.Forms.Label CbTrackLabel;
		private System.Windows.Forms.Label CrTrackLabel;
		private System.Windows.Forms.Label Y0TickLabel;
		private System.Windows.Forms.Label Y4TickLabel;
		private System.Windows.Forms.Label Y8TickLabel;
		private System.Windows.Forms.Label U8TickLabel;
		private System.Windows.Forms.Label U4TickLabel;
		private System.Windows.Forms.Label U0TickLabel;
		private System.Windows.Forms.Label V8TickLabel;
		private System.Windows.Forms.Label V4TickLabel;
		private System.Windows.Forms.Label V0TickLabel;
		private System.Windows.Forms.TabControl TaskPanel;
		private System.Windows.Forms.TabPage ConvertTaskPage;
		private System.Windows.Forms.TabPage PSNRTaskPage;
		private System.Windows.Forms.TabPage GrayscaleTaskPage;
		public System.Windows.Forms.RadioButton GrayscaleEQRadio;
		public System.Windows.Forms.RadioButton GrayscaleCCIRRadio;
		public System.Windows.Forms.RadioButton GrayscaleCMPRadio;
		public System.Windows.Forms.Label GrayscaleRightImageLabel;
		public System.Windows.Forms.Label GrayscaleLeftImageLabel;
		private System.Windows.Forms.TabPage LBGTaskPage;
		private System.Windows.Forms.Label LBGCountLabel;
		public System.Windows.Forms.Label LBGCountValueLabel;
		public System.Windows.Forms.TrackBar LBGCountTrackBar;
		public System.Windows.Forms.Button LBGApplyButton;
		private System.Windows.Forms.TabPage LBGPositionTaskPage;
		private System.Windows.Forms.Label LBGPRelationLabel;
		public System.Windows.Forms.NumericUpDown LBGPCountValue;
		public System.Windows.Forms.Button LBGPApplyButton;
		public System.Windows.Forms.Label LBGPRelationValueLabel;
		public System.Windows.Forms.TrackBar LBGPRatioTrackBar;
		private System.Windows.Forms.Label LBGPCountLabel;
		private System.Windows.Forms.TabPage JPEGTaskPage;
		public System.Windows.Forms.ComboBox JPEGDownsamplingBox;
		private System.Windows.Forms.Label JPEGDownsamplingLabel;
		public System.Windows.Forms.Button JPEGApplyButton;
		public System.Windows.Forms.NumericUpDown JPEGQuantizationParamBValue;
		public System.Windows.Forms.Label JPEGQuantizationParamBLabel;
		public System.Windows.Forms.NumericUpDown JPEGQuantizationParamAValue;
		public System.Windows.Forms.Label JPEGQuantizationParamALabel;
		public System.Windows.Forms.Button JPEGSaveButton;
		public System.Windows.Forms.ComboBox JPEGQuantizationBox;
		private System.Windows.Forms.Label JPEGQuantizationLabel;
		public System.Windows.Forms.Label JPEGSizeLabel;
		public System.Windows.Forms.Button JPEGOpenButton;
		public System.Windows.Forms.NumericUpDown JPEGQuantizationParamDValue;
		public System.Windows.Forms.Label JPEGQuantizationParamDLabel;
		public System.Windows.Forms.NumericUpDown JPEGQuantizationParamCValue;
		public System.Windows.Forms.Label JPEGQuantizationParamCLabel;
		private System.Windows.Forms.TabPage WaveletTaskPage;
		public System.Windows.Forms.NumericUpDown WaveletDepthValue;
		public System.Windows.Forms.ComboBox WaveletTypeBox;
		private System.Windows.Forms.Label WaveletTypeLabel;
		private System.Windows.Forms.Label WaveletDepthLabel;
		private System.Windows.Forms.Label WaveletCompressRatioLabel;
		public System.Windows.Forms.Label WaveletCompressRatioValue;
		public System.Windows.Forms.NumericUpDown WaveletThresholdValue;
		private System.Windows.Forms.Label WaveletThresholdLabel;
		private System.Windows.Forms.Label WaveletCompressedSizeLabel;
		public System.Windows.Forms.Label WaveletCompressedSizeValue;
	}
}

