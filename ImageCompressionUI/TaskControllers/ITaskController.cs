namespace ImageCompressionUI.TaskControllers
{
	public interface ITaskController
	{
		void BindEvents(MainForm mainForm);
		void Initialize();
		void UnbindEvents(MainForm mainForm);
	}
}