using System;

namespace ImageCompressionUI.TaskControllers
{
	public abstract class ConvertController : ITaskController
	{
		protected MainFormState State;

		public void BindEvents(MainForm mainForm)
		{
			mainForm.OpenRightImageButton.Enabled = false;

			State = mainForm.State;
			State.LeftImageUpdated += OnLeftImageUpdated;

			BindInternalEvents(mainForm);
		}

		public void Initialize()
		{
			State.SetLeftBitmap(State.LeftImage);
			InitializeInternal();
		}

		public virtual void UnbindEvents(MainForm mainForm)
		{
			State.LeftImageUpdated -= OnLeftImageUpdated;

			mainForm.OpenRightImageButton.Enabled = true;
			UnbindInternalEvents(mainForm);
		}

		private void OnLeftImageUpdated(object sender, EventArgs eventArgs)
		{
			Initialize();
		}

		protected abstract void BindInternalEvents(MainForm mainForm);
		protected abstract void UnbindInternalEvents(MainForm mainForm);
		protected abstract void InitializeInternal();
	}
}