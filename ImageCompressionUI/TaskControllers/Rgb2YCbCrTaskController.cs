using System;
using System.Windows.Forms;
using ImageCompressionUI.ImageProcessing;

namespace ImageCompressionUI.TaskControllers
{
	public class Rgb2YCbCrTaskController : ConvertController
	{
		private readonly TruncateFilter truncateFilter = new TruncateFilter(8, 8, 8);

		protected override void BindInternalEvents(MainForm mainForm)
		{
			mainForm.YTrackBar.Scroll += YTrackBar_Scroll;
			mainForm.CbTrackBar.Scroll += CbTrackBar_Scroll;
			mainForm.CrTrackBar.Scroll += CrTrackBar_Scroll;
		}

		protected override void InitializeInternal()
		{
			UpdateConvertedImage();
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			mainForm.YTrackBar.Scroll -= YTrackBar_Scroll;
			mainForm.CbTrackBar.Scroll -= CbTrackBar_Scroll;
			mainForm.CrTrackBar.Scroll -= CrTrackBar_Scroll;
		}

		private void YTrackBar_Scroll(object sender, EventArgs e)
		{
			truncateFilter.RSize = ((TrackBar) sender).Value;
			UpdateConvertedImage();
		}

		private void CbTrackBar_Scroll(object sender, EventArgs e)
		{
			truncateFilter.GSize = ((TrackBar)sender).Value;
			UpdateConvertedImage();
		}

		private void CrTrackBar_Scroll(object sender, EventArgs e)
		{
			truncateFilter.BSize = ((TrackBar)sender).Value;
			UpdateConvertedImage();
		}

		private void UpdateConvertedImage()
		{
			if (State.LeftImage == null) return;
			State.RightImage = State.LeftImage
				.Clone()
				.Apply(RgbToYCbCr.Instance)
				.Apply(truncateFilter)
				.Apply(YCbCrToRgb.Instance);
			State.UpdatePSNR(State.LeftImage, State.RightImage);
			State.SetRightBitmap(State.RightImage);
		}
	}
}