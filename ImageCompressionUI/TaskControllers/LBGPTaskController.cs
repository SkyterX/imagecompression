using System;
using System.Windows.Forms;
using ImageCompressionUI.ImageProcessing;

namespace ImageCompressionUI.TaskControllers
{
	public class LBGPTaskController : ConvertController
	{
		private const double relationDivisor = 20.0;
		private Label relationLabel;
		private LBGPFilter lbgpFilter;

		protected override void BindInternalEvents(MainForm mainForm)
		{
			mainForm.LBGPRatioTrackBar.Scroll += LBGPRatioTrackBarOnScroll;
			mainForm.LBGPApplyButton.Click += LBGPApplyButtonOnClick;
			mainForm.LBGPCountValue.ValueChanged += LBGPCountValueOnValueChanged;

			relationLabel = mainForm.LBGPRelationValueLabel;
			var relation = mainForm.LBGPRatioTrackBar.Value / relationDivisor;
			relationLabel.Text = relation.ToString("#0.00");
			lbgpFilter = new LBGPFilter((int) mainForm.LBGPCountValue.Value, relation);
		}

		private void LBGPCountValueOnValueChanged(object sender, EventArgs eventArgs)
		{
			var numeric = (NumericUpDown) sender;
			lbgpFilter.ColorsCount = (int) numeric.Value;
		}

		protected override void InitializeInternal()
		{
			State.SetRightBitmap(State.LeftImage);
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			mainForm.LBGCountTrackBar.Scroll -= LBGPRatioTrackBarOnScroll;
			mainForm.LBGApplyButton.Click -= LBGPApplyButtonOnClick;
		}

		private void LBGPApplyButtonOnClick(object sender, EventArgs eventArgs)
		{
			var button = (Button) sender;
			button.Enabled = false;
			UpdateConvertedImage();
			button.Enabled = true;
		}

		private void LBGPRatioTrackBarOnScroll(object sender, EventArgs eventArgs)
		{
			var trackBar = (TrackBar) sender;
			var relation = trackBar.Value / relationDivisor;
			relationLabel.Text = relation.ToString("#0.00");
			lbgpFilter.Color2PositionRelation = relation;
		}

		private void UpdateConvertedImage()
		{
			if (State.LeftImage == null) return;
			State.RightImage = State.LeftImage
				.Clone()
				.Apply(lbgpFilter);
			State.UpdatePSNR(State.LeftImage, State.RightImage);
			State.SetRightBitmap(State.RightImage);
		}
	}
}