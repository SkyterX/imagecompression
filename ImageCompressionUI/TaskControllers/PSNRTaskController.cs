using System;

namespace ImageCompressionUI.TaskControllers
{
	public class PSNRTaskController : ITaskController
	{
		private MainFormState state;

		public void BindEvents(MainForm mainForm)
		{
			state = mainForm.State;
			state.LeftImageUpdated += OnLeftImageUpdated;
			state.RightImageUpdated += OnRightImageUpdated;
		}

		private void OnLeftImageUpdated(object sender, EventArgs eventArgs)
		{
			state.SetLeftBitmap(state.LeftImage);
			state.UpdatePSNR(state.LeftImage, state.RightImage);
		}

		private void OnRightImageUpdated(object sender, EventArgs eventArgs)
		{
			state.SetRightBitmap(state.RightImage);
			state.UpdatePSNR(state.LeftImage, state.RightImage);
		}

		public void Initialize()
		{
			state.SetLeftBitmap(state.LeftImage);
			state.SetRightBitmap(state.RightImage);
			state.UpdatePSNR(state.LeftImage, state.RightImage);
		}

		public void UnbindEvents(MainForm mainForm)
		{
			state.LeftImageUpdated -= OnLeftImageUpdated;
			state.RightImageUpdated -= OnRightImageUpdated;
		}
	}
}