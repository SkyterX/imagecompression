﻿using System;
using System.Windows.Forms;
using ImageCompressionUI.ImageProcessing;

namespace ImageCompressionUI.TaskControllers
{
	public class GrayscaleTaskController : ConvertController
	{
		private enum ViewType
		{
			EqualWeight,
			CCIR,
			Compare
		}

		private ViewType currentViewType;
		private RgbImage equalWeightGray;
		private RgbImage ccirGray;
		private Label leftImageLabel;
		private Label rightImageLabel;

		protected override void BindInternalEvents(MainForm mainForm)
		{
			mainForm.GrayscaleEQRadio.Tag = ViewType.EqualWeight;
			mainForm.GrayscaleCCIRRadio.Tag = ViewType.CCIR;
			mainForm.GrayscaleCMPRadio.Tag = ViewType.Compare;
			mainForm.GrayscaleEQRadio.CheckedChanged += OnRadioButtonChecked;
			mainForm.GrayscaleCCIRRadio.CheckedChanged += OnRadioButtonChecked;
			mainForm.GrayscaleCMPRadio.CheckedChanged += OnRadioButtonChecked;


			leftImageLabel = mainForm.GrayscaleLeftImageLabel;
			rightImageLabel = mainForm.GrayscaleRightImageLabel;
			if (mainForm.GrayscaleEQRadio.Checked) currentViewType = ViewType.EqualWeight;
			if (mainForm.GrayscaleCCIRRadio.Checked) currentViewType = ViewType.CCIR;
			if (mainForm.GrayscaleCMPRadio.Checked) currentViewType = ViewType.Compare;
		}

		protected override void InitializeInternal()
		{
			CreateGrayscale();
			UpdateConvertedImage();
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			mainForm.GrayscaleEQRadio.CheckedChanged -= OnRadioButtonChecked;
			mainForm.GrayscaleCCIRRadio.CheckedChanged -= OnRadioButtonChecked;
			mainForm.GrayscaleCMPRadio.CheckedChanged -= OnRadioButtonChecked;
		}

		private void CreateGrayscale()
		{
			equalWeightGray = State.LeftImage.Clone().Apply(Grayscale.EqualWeight);
			ccirGray = State.LeftImage.Clone().Apply(Grayscale.CCIR);
		}

		private void OnRadioButtonChecked(object sender, EventArgs eventArgs)
		{
			var radioButton = sender as RadioButton;
			if (radioButton == null || !radioButton.Checked) return;
			currentViewType = (ViewType) radioButton.Tag;
			leftImageLabel.Visible = currentViewType == ViewType.Compare;
			rightImageLabel.Visible = currentViewType == ViewType.Compare;
			UpdateConvertedImage();
		}

		private void UpdateConvertedImage()
		{
			switch (currentViewType)
			{
				case ViewType.EqualWeight:
					State.SetLeftBitmap(State.LeftImage);
					State.SetRightBitmap(equalWeightGray);
					State.UpdatePSNR(State.LeftImage, equalWeightGray);
					break;
				case ViewType.CCIR:
					State.SetLeftBitmap(State.LeftImage);
					State.SetRightBitmap(ccirGray);
					State.UpdatePSNR(State.LeftImage, ccirGray);
					break;
				case ViewType.Compare:
					State.SetLeftBitmap(equalWeightGray);
					State.SetRightBitmap(ccirGray);
					State.UpdatePSNR(equalWeightGray, ccirGray);
					break;
			}
		}
	}
}