﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using NUnit.Framework;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public static class MatrixOrdering
	{
		public static IEnumerable<Position> EnumerateZigZag<T>(this Matrix<T> matrix)
			where T : struct, IEquatable<T>, IFormattable
		{
			return GetZigZagOrdering(matrix.RowCount, matrix.ColumnCount);
		} 

		public static IEnumerable<Position> GetZigZagOrdering(int order)
		{
			return GetZigZagOrdering(order, order);
		}

		public static IEnumerable<Position> GetZigZagOrdering(int rows, int cols)
		{
			return Enumerable
				.Range(0, rows)
				.SelectMany(x => Enumerable
					.Range(0, cols)
					.Select(y => new Position(x, y)))
				.OrderBy(pos => pos.X + pos.Y)
				.ThenByDescending(pos => (pos.X + pos.Y)%2 == 0 ? pos.X : pos.Y);
		}

		public static IEnumerable<T> ZigZagUnfold<T>(this Matrix<T> matrix)
			where T : struct, IEquatable<T>, IFormattable
		{
			return GetZigZagOrdering(matrix.RowCount, matrix.ColumnCount)
				.Select(pos => matrix[pos.X, pos.Y]);
		}

		public static Matrix<T> ZigZagFold<T>(this IEnumerable<T> source, int order)
			where T : struct, IEquatable<T>, IFormattable
		{
			return ZigZagFold(source, Matrix<T>.Build.Dense(order, order));
		}

		public static Matrix<T> ZigZagFold<T>(this IEnumerable<T> source, int rows, int cols)
			where T : struct, IEquatable<T>, IFormattable
		{
			return ZigZagFold(source, Matrix<T>.Build.Dense(rows, cols));
		}

		public static Matrix<T> ZigZagFold<T>(this IEnumerable<T> source, Matrix<T> matrix)
			where T : struct, IEquatable<T>, IFormattable
		{
			var order = GetZigZagOrdering(matrix.RowCount, matrix.ColumnCount)
				.Zip(source, (pos, value) => new {pos.X, pos.Y, value});
			foreach (var data in order)
			{
				matrix[data.X, data.Y] = data.value;
			}
			return matrix;
		}

		public static IEnumerable<Position> EnumerateBlocks<T>(this Matrix<T> matrix, int blockSize)
			where T : struct, IEquatable<T>, IFormattable
		{
			if (matrix.RowCount % blockSize != 0 || matrix.ColumnCount % blockSize != 0)
				throw new NotSupportedException("Matrix size must be multiple of block size");
			for (var x = 0; x < matrix.RowCount; x += blockSize)
			{
				for (var y = 0; y < matrix.ColumnCount; y += blockSize)
				{
					yield return new Position(x, y);
				}
			}
		}

		[Serializable]
		public class Position
		{
			public readonly int X;
			public readonly int Y;

			public Position(int x, int y)
			{
				X = x;
				Y = y;
			}
		}
	}

	[TestFixture]
	public class ZigZagOrdering_Test
	{
		[Test]
		public void Test()
		{
			var matrix = Matrix<double>.Build.DenseOfArray(new double[,]
			{
				{1, 3, 4, 10},
				{2, 5, 9, 11},
				{6, 8, 12, 15},
				{7, 13, 14, 16},
			});
			var array = Enumerable.Range(1, matrix.RowCount*matrix.ColumnCount).ToArray();
			var unfoldArray = matrix.ZigZagUnfold().ToArray();
			var foldMatrix = unfoldArray.ZigZagFold(matrix.RowCount, matrix.ColumnCount);
			Assert.That(unfoldArray, Is.EquivalentTo(array));
			Assert.That(foldMatrix, Is.EqualTo(matrix));
			Console.Out.WriteLine("{0}", matrix.ToMatrixString());
		}
	}
}