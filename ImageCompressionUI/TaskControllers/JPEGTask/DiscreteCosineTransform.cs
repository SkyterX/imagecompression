﻿using System;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class DiscreteCosineTransform : IMatrixFilter
	{
		public static readonly DiscreteCosineTransform DCT8 = new DiscreteCosineTransform(8, false);
		public static readonly DiscreteCosineTransform DCT8Inverse = new DiscreteCosineTransform(8, true);
		public static readonly DiscreteCosineTransform DCT16 = new DiscreteCosineTransform(16, false);
		public static readonly DiscreteCosineTransform DCT16Inverse = new DiscreteCosineTransform(16, true);

		private readonly bool inverse;
		private readonly Matrix<double> transform; 

		public DiscreteCosineTransform(int size, bool inverse)
		{
			this.inverse = inverse;
			transform = GenerateTransformMatrix(size);
		}

		private static Matrix<double> GenerateTransformMatrix(int size)
		{
			var matrix = Matrix<double>.Build.Dense(size, size, 1);
			for (var q = 0; q < size; q++)
			{
				matrix[0, q] = Math.Sqrt(1.0/size);
			}
			for (int p = 1; p < size; p++)
			{
				for (var q = 0; q < size; q++)
				{
					matrix[p, q] = Math.Sqrt(2.0 / size)*Math.Cos((Math.PI * (2 * q + 1) * p) / (2 * size));
				}
			}
			return matrix;
		}

		public void Apply(Matrix<double> matrix)
		{
			if (inverse)
			{
				transform.TransposeThisAndMultiply(matrix, matrix);
				matrix.Multiply(transform, matrix);
			}
			else
			{
				transform.Multiply(matrix, matrix);
				matrix.TransposeAndMultiply(transform, matrix);
			}
		}
	}
}