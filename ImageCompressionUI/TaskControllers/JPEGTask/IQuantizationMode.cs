namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public interface IQuantizationMode
	{
		string DisplayName { get; }
		string[] ParamNames { get; }
		IQuantization[] GetQuantizations(params double[] paramValues);
	}

	public class NoQuantizationMode : IQuantizationMode
	{
		public string DisplayName { get { return "No quantization"; } }
		public string[] ParamNames { get {return new string[0];} }
		public IQuantization[] GetQuantizations(params double[] paramValues)
		{
			return new IQuantization[]
			{
				QuantizationStub.Instance,
				QuantizationStub.Instance,
				QuantizationStub.Instance,
			};
		}
	}
}