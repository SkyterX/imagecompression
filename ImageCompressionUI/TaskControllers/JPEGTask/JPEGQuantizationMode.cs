using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class JPEGQuantizationMode : IQuantizationMode
	{
		private static readonly string[] paramNames = { "C" };

		public static readonly Matrix<double> JPEGDefaultY = Matrix<double>.Build.DenseOfArray(new double[,]
		{
			{16, 11, 10, 16, 24, 40, 51, 61},
			{12, 12, 14, 19, 26, 58, 60, 55},
			{14, 13, 16, 24, 40, 57, 69, 56},
			{14, 17, 22, 29, 51, 87, 80, 62},
			{18, 22, 37, 56, 68, 109, 103, 77},
			{24, 35, 55, 64, 81, 104, 113, 92},
			{49, 64, 78, 87, 103, 121, 120, 101},
			{72, 92, 95, 98, 112, 100, 103, 99}
		});

		public static readonly Matrix<double> JPEGDefaultC = Matrix<double>.Build.DenseOfArray(new double[,]
		{
			{17, 18, 24, 47, 99, 99, 99, 99},
			{18, 21, 26, 66, 99, 99, 99, 99},
			{24, 26, 56, 99, 99, 99, 99, 99},
			{47, 66, 99, 99, 99, 99, 99, 99},
			{99, 99, 99, 99, 99, 99, 99, 99},
			{99, 99, 99, 99, 99, 99, 99, 99},
			{99, 99, 99, 99, 99, 99, 99, 99},
			{99, 99, 99, 99, 99, 99, 99, 99}
		});

		public string DisplayName
		{
			get { return "JPEG default * Const"; }
		}

		public string[] ParamNames
		{
			get { return paramNames; }
		}

		public IQuantization[] GetQuantizations(params double[] paramValues)
		{
			return new IQuantization[]
			{
				new MatrixQuantization(JPEGDefaultY.Multiply(paramValues[0])),
				new MatrixQuantization(JPEGDefaultC.Multiply(paramValues[0])),
				new MatrixQuantization(JPEGDefaultC.Multiply(paramValues[0])),
			};
		}
	}
}