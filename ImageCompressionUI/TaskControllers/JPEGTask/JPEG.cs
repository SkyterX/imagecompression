﻿using System;
using ImageCompressionUI.ImageProcessing;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public static class JPEG
	{
		private const int blockSize = 8;

		public static JPEGStorage Encode(RgbImage image, SamplingRate[] samplingRates,
			IQuantization[] quantizations)
		{
			var matrices = MatrixConverter.YCbCr.Convert(image, samplingRates);
			for (var idx = 0; idx < matrices.Length; idx++)
			{
				quantizations[idx].MatrixSize = blockSize;
				var filter = DiscreteCosineTransform.DCT8
					.Then(quantizations[idx].ToFilter());
				matrices[idx].ApplyBlockFilter(blockSize, filter);
			}
			var arrays = Array.ConvertAll(matrices, matrix => matrix.ToArray(blockSize));
			return new JPEGStorage
			{
				ComponentArrays = arrays,
				Quantizations = quantizations,
				SamplingRates = samplingRates
			};
		}

		public static RgbImage Decode(JPEGStorage storage)
		{
			var arrays = storage.ComponentArrays;
			var quantizations = storage.Quantizations;
			var samplingRates = storage.SamplingRates;

			var matrices = Array.ConvertAll(arrays, byteArray => byteArray.ToMatrix());
			for (var idx = 0; idx < matrices.Length; idx++)
			{
				quantizations[idx].MatrixSize = blockSize;
				var filter = quantizations[idx].ToInverse()
					.Then(DiscreteCosineTransform.DCT8Inverse);
				matrices[idx].ApplyBlockFilter(blockSize, filter);
			}
			return MatrixConverter.YCbCr.Convert(matrices, samplingRates);
		}
	}
}