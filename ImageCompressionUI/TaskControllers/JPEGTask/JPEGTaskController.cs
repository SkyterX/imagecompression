using System;
using System.IO;
using System.Windows.Forms;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class JPEGTaskController : ConvertController
	{
		private readonly DownsamplingController downsampling;
		private readonly QuantizationController quantization;
		private Label sizeLabel;

		public JPEGTaskController()
		{
			downsampling = new DownsamplingController();
			quantization = new QuantizationController();
		}

		protected override void BindInternalEvents(MainForm mainForm)
		{
			downsampling.BindEvents(mainForm);
			quantization.BindEvents(mainForm);
			sizeLabel = mainForm.JPEGSizeLabel;

			mainForm.JPEGApplyButton.Click += JpegApplyButtonOnClick;
			mainForm.JPEGSaveButton.Click += JpegSaveButtonOnClick;
			mainForm.JPEGOpenButton.Click += JpegOpenButtonOnClick;
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			downsampling.UnbindEvents(mainForm);
			quantization.UnbindEvents(mainForm);
			mainForm.JPEGApplyButton.Click -= JpegApplyButtonOnClick;
			mainForm.JPEGSaveButton.Click -= JpegSaveButtonOnClick;
			mainForm.JPEGOpenButton.Click -= JpegOpenButtonOnClick;
		}

		protected override void InitializeInternal()
		{
			downsampling.Initialize();
			quantization.Initialize();
			State.ClearRightBitmap();
		}

		private void JpegSaveButtonOnClick(object sender, EventArgs eventArgs)
		{
			if (State.LeftImage == null) return;

			var bytes = JPEG
				.Encode(State.LeftImage, downsampling.SamplingRates, quantization.Quantizations)
				.Serialize();

			var saveFileDialog = new SaveFileDialog
			{
				AddExtension = true,
				DefaultExt = ".myjpg",
				Filter = "Jpeg files (*.myjpg)|*.myjpg",
				Title = "Save jpeg"
			};
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					File.WriteAllBytes(saveFileDialog.FileName, bytes);
				}
				catch (Exception e)
				{
				}
			}
		}

		private void JpegOpenButtonOnClick(object sender, EventArgs eventArgs)
		{
			var openDialog = new OpenFileDialog
			{
				Title = "Open jpeg",
				Filter = "Jpeg files (*.myjpg)|*.myjpg"
			};
			if (openDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					var bytes = File.ReadAllBytes(openDialog.FileName);
					UpdateRightImage(bytes);
				}
				catch (Exception e)
				{

				}
			}
		}

		private void JpegApplyButtonOnClick(object sender, EventArgs eventArgs)
		{
			var button = (Button)sender;
			button.Enabled = false;
			UpdateConvertedImage();
			button.Enabled = true;
		}

		private void UpdateConvertedImage()
		{
			if (State.LeftImage == null) return;

			var encoded = JPEG.Encode(State.LeftImage, downsampling.SamplingRates, quantization.Quantizations);

			var bytes = encoded.Serialize();
			UpdateRightImage(bytes);
		}

		private void UpdateRightImage(byte[] bytes)
		{
			sizeLabel.Text = Formatter.FormatMemory(bytes.LongLength);

			State.RightImage = JPEG.Decode(JPEGStorage.Deserialize(bytes));

			State.UpdatePSNR(State.LeftImage, State.RightImage);
			State.SetRightBitmap(State.RightImage);
		}
	}
}