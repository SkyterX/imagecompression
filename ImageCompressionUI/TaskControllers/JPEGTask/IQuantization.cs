using System;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public interface IQuantization
	{
		int MatrixSize { get; set; }
		void Apply(Matrix<double> matrix);
		void Invert(Matrix<double> matrix);
	}

	public static class QuantizationExtensions
	{
		public static IMatrixFilter ToFilter(this IQuantization quantization)
		{
			return new QuantizationFilter(quantization, false);
		}

		public static IMatrixFilter ToInverse(this IQuantization quantization)
		{
			return new QuantizationFilter(quantization, true);
		}

		[Serializable]
		private class QuantizationFilter : IMatrixFilter
		{
			private readonly IQuantization quantization;
			private readonly bool inverse;

			public QuantizationFilter(IQuantization quantization, bool inverse)
			{
				this.quantization = quantization;
				this.inverse = inverse;
			}

			public void Apply(Matrix<double> matrix)
			{
				if (inverse)
				{
					quantization.Invert(matrix);
				}
				else
				{
					quantization.Apply(matrix);
				}
			}
		}
	}

	[Serializable]
	public class QuantizationStub : IQuantization
	{
		public static readonly QuantizationStub Instance = new QuantizationStub();

		private QuantizationStub()
		{
		}

		public int MatrixSize { get; set; }

		public void Apply(Matrix<double> matrix)
		{
		}

		public void Invert(Matrix<double> matrix)
		{
		}
	}
}