namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class FormulaMatrixQuantizationMode : IQuantizationMode
	{
		private static readonly string[] paramNames = { "\u03b1Y", "\u03b3Y", "\u03b1C", "\u03b3C" };

		public string DisplayName { get { return "Q_x,y = \u03b1(1+\u03b3*(x+y+2))"; } }
		public string[] ParamNames { get { return paramNames; } }
		public IQuantization[] GetQuantizations(params double[] paramValues)
		{
			var qY = new FormulaMatrixQuantization(paramValues[0], paramValues[1]);
			var qC = new FormulaMatrixQuantization(paramValues[2], paramValues[3]);
			return new IQuantization[] { qY, qC, qC };
		}
	}
}