﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using NUnit.Framework;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public static class MatrixStorage
	{
		public static Array ToArray(this Matrix<double> matrix, int blockSize)
		{
			var data = new short[matrix.RowCount*matrix.ColumnCount];
			var ordering = MatrixOrdering.GetZigZagOrdering(blockSize).ToArray();
			Parallel.ForEach(matrix.EnumerateBlocks(blockSize), block =>
			{
				var dataIdx = block.X*matrix.ColumnCount + block.Y*blockSize;
				foreach (var position in ordering)
				{
					data[dataIdx++] = ToShort(matrix[position.X + block.X, position.Y + block.Y]);
				}
			});
			return new Array
			{
				Data = data,
				RowCount = matrix.RowCount,
				ColumnCount = matrix.ColumnCount,
				BlockSize = blockSize
			};
		}

		public static Matrix<double> ToMatrix(this Array storage)
		{
			var matrix = Matrix<double>.Build.Dense(storage.RowCount, storage.ColumnCount);
			var ordering = MatrixOrdering.GetZigZagOrdering(storage.BlockSize).ToArray();
			Parallel.ForEach(matrix.EnumerateBlocks(storage.BlockSize), block =>
			{
				var dataIdx = block.X*matrix.ColumnCount + block.Y*storage.BlockSize;
				foreach (var position in ordering)
				{
					matrix[position.X + block.X, position.Y + block.Y] = storage.Data[dataIdx++];
				}
			});
			return matrix;
		}

		private static short ToShort(double value)
		{
			var intVal = (int) Math.Round(value);
			if (intVal > short.MaxValue) return short.MaxValue;
			if (intVal < short.MinValue) return short.MinValue;
			return (short) intVal;
		}

		[Serializable]
		public class Array
		{
			public short[] Data;
			public int RowCount;
			public int ColumnCount;
			public int BlockSize;
		}
	}

	[TestFixture]
	public class MatrixStorage_Test
	{
		[Test]
		public void Test()
		{
			const int blockSize = 2;
			const int blockRows = 3;
			const int blockColumns = 3;
			var rows = blockRows*blockSize;
			var columns = blockColumns*blockSize;

			var matrixArr = new double[rows, columns];
			var idx = 0;
			for (var x = 0; x < rows; x++)
			{
				for (var y = 0; y < columns; y++)
				{
					matrixArr[x, y] = ++idx;
				}
			}

			var matrix = Matrix<double>.Build.DenseOfArray(matrixArr);

			Console.Out.WriteLine("{0}", matrix.ToMatrixString());

			var storage = matrix.ToArray(blockSize);

			var array = string.Join(", ", storage.Data.Select(x => x.ToString()).ToArray());

			Console.Out.WriteLine("{0}", array);

			Assert.That(matrix, Is.EqualTo(storage.ToMatrix()));
		}
	}
}