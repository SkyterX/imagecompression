using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ICSharpCode.SharpZipLib.GZip;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	[Serializable]
	public class JPEGStorage
	{
		public MatrixStorage.Array[] ComponentArrays;
		public SamplingRate[] SamplingRates;
		public IQuantization[] Quantizations;

		public byte[] Serialize()
		{
			var formatter = new BinaryFormatter();
			var stream = new MemoryStream();
			using (var zipStream = new GZipOutputStream(stream))
			{
				formatter.Serialize(zipStream, this);
			}
			return stream.ToArray();
		}

		public static JPEGStorage Deserialize(byte[] data)
		{
			var formatter = new BinaryFormatter();
			var stream = new MemoryStream(data);
			using (var zipStream = new GZipInputStream(stream))
			{
				return formatter.Deserialize(zipStream) as JPEGStorage;
			}
		}
	}
}