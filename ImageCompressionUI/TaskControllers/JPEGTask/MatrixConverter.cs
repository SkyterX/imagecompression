﻿using System;
using ImageCompressionUI.ImageProcessing;
using ImageCompressionUI.Util;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class MatrixConverter
	{
		private static readonly Matrix<double> RgbToYCbCrTransform =
			Matrix.Build.DenseOfArray(new[,]
			{
				{0.299, 0.587, 0.114},
				{-0.168736, -0.331264, 0.5},
				{0.5, -0.418688, -0.081313}
			});

		private static readonly Matrix<double> YCbCrToRgbTransform =
			Matrix.Build.DenseOfArray(new[,]
			{
				{1, 0, 1.402},
				{1, - 0.34414, -0.71414},
				{1, 1.772, 0}
			});


		private readonly Matrix<double> transform;
		private readonly Matrix<double> inverse;
		private readonly Vector<double> shift;

		public static readonly MatrixConverter YCbCr = new MatrixConverter(
			RgbToYCbCrTransform, YCbCrToRgbTransform,
			Vector.Build.DenseOfArray(new double[] {0, 128, 128}));


		public MatrixConverter(Matrix<double> transform, Matrix<double> inverse, Vector<double> shift)
		{
			this.transform = transform;
			this.inverse = inverse;
			this.shift = shift;
		}

		public Matrix<double>[] Convert(RgbImage image, SamplingRate[] sr)
		{
			var matrices = new Matrix<double>[3]
				.Fill(i => Matrix.Build.Dense(
					image.Width/sr[i].Horizontal,
					image.Height/sr[i].Vertical,
					0));

			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					var color = image[x, y];
					var vector = Vector.Build.DenseOfArray(new double[] {color.R, color.G, color.B});
					transform.Multiply(vector, vector);
					vector.Add(shift, vector);
					for (var i = 0; i < matrices.Length; i++)
					{
						if (x%sr[i].Horizontal != 0 || y%sr[i].Vertical != 0) continue;
						matrices[i][x/sr[i].Horizontal, y/sr[i].Vertical] = vector[i];
					}
				}
			}
			return matrices;
		}

		public RgbImage Convert(Matrix<double>[] matrices, SamplingRate[] sr)
		{
			var image = new RgbImage(matrices[0].RowCount*sr[0].Horizontal, matrices[0].ColumnCount*sr[0].Vertical);
			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					var vector = Vector.Build.Dense(matrices.Length, i =>
						matrices[i][x/sr[i].Horizontal, y/sr[i].Vertical]);
					vector.Subtract(shift, vector);
					inverse.Multiply(vector, vector);
					image[x, y] = new RgbColor(ToByte(vector[0]), ToByte(vector[1]), ToByte(vector[2]));
				}
			}
			return image;
		}

		private byte ToByte(double value)
		{
			var intVal = (int) Math.Round(value);
			if (intVal > 255) return 255;
			if (intVal < 0) return 0;
			return (byte) intVal;
		}
	}
}