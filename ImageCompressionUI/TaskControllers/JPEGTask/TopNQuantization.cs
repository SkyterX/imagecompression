using System;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	[Serializable]
	public class TopNQuantization : IQuantization
	{
		private readonly int n;
		private MatrixOrdering.Position[] order;
		private int matrixSize;

		public TopNQuantization(int n)
		{
			this.n = n;
			MatrixSize = 8;
			
		}

		public int MatrixSize
		{
			get { return matrixSize; }
			set
			{
				matrixSize = value;
				order = MatrixOrdering.GetZigZagOrdering(matrixSize).ToArray();	
			}
		}

		public void Apply(Matrix<double> matrix)
		{
			var threshold = matrix
				.Enumerate()
				.OrderByDescending(Math.Abs)
				.Skip(n-1)
				.FirstOrDefault();
			int cnt = 0;
			for (var i = 0; i < order.Length; i++)
			{
				if (Math.Abs(matrix[order[i].X, order[i].Y]) < threshold || cnt == n)
					matrix[order[i].X, order[i].Y] = 0;
				else
					cnt++;
			}
		}

		public void Invert(Matrix<double> matrix)
		{
		}
	}
}