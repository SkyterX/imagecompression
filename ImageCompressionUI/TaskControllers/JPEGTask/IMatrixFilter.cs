﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public interface IMatrixFilter
	{
		void Apply(Matrix<double> matrix);
	}

	public class CombinedFilter : IMatrixFilter
	{
		private readonly IMatrixFilter[] filters;

		public CombinedFilter(params IMatrixFilter[] filters)
		{
			this.filters = filters;
		}

		public void Apply(Matrix<double> matrix)
		{
			foreach (var matrixFilter in filters)
			{
				matrixFilter.Apply(matrix);
			}
		}
	}

	public static class MatrixFilterExtensions
	{
		public static Matrix<double> Apply(this Matrix<double> m, IMatrixFilter filter)
		{
			filter.Apply(m);
			return m;
		}

		public static IMatrixFilter Then(this IMatrixFilter filter, IMatrixFilter nextFilter)
		{
			return new CombinedFilter(filter, nextFilter);
		}

		public static void ApplyBlockFilter(this Matrix<double> matrix, int blockSize, IMatrixFilter filter)
		{
			Parallel.ForEach(matrix.EnumerateBlocks(blockSize), block =>
			{
				var x = block.X;
				var y = block.Y;
				matrix.SetSubMatrix(x, y, matrix
					.SubMatrix(x, blockSize, y, blockSize)
					.Apply(filter));
			});
		}
	}
}