using System;
using System.Windows.Forms;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class DownsamplingController : ITaskController
	{
		private static readonly SamplingRate[] samplingRates =
		{
			SamplingRate.Default,
			new SamplingRate(2, 1, "4:2:2 (2h1v)"),
			new SamplingRate(1, 2, "4:2:2 (1h2v)"),
			new SamplingRate(2, 2, "4:1:1 (2h2v)")
		};

		private ComboBox downsamplingBox;

		public SamplingRate[] SamplingRates
		{
			get
			{
				return new[]
				{
					SamplingRate.Default,
					samplingRates[downsamplingBox.SelectedIndex],
					samplingRates[downsamplingBox.SelectedIndex]
				};
			}
		}

		public void BindEvents(MainForm mainForm)
		{
			downsamplingBox = mainForm.JPEGDownsamplingBox;
		}

		public void Initialize()
		{
			downsamplingBox.Items.Clear();
			foreach (var samplingRate in samplingRates)
			{
				downsamplingBox.Items.Add(samplingRate.DisplayName);
			}
			downsamplingBox.SelectedIndex = 0;
		}

		public void UnbindEvents(MainForm mainForm)
		{
		}
	}

	[Serializable]
	public struct SamplingRate
	{
		public static readonly SamplingRate Default = new SamplingRate(1, 1, "4:4:4");

		public readonly int Horizontal;
		public readonly int Vertical;
		public readonly string DisplayName;

		public SamplingRate(int horizontal, int vertical, string displayName)
		{
			Horizontal = horizontal;
			Vertical = vertical;
			DisplayName = displayName;
		}
	}
}