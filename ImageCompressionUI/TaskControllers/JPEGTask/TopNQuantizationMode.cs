namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class TopNQuantizationMode : IQuantizationMode
	{
		private static readonly string[] paramNames = {"NY", "NC"};

		public string DisplayName { get { return "Leave top N values"; } }
		public string[] ParamNames { get { return paramNames; }}
		public IQuantization[] GetQuantizations(params double[] paramValues)
		{
			var qY = new TopNQuantization((int)paramValues[0]);
			var qC = new TopNQuantization((int)paramValues[1]);
			return new IQuantization[] {qY, qC, qC};
		}
	}
}