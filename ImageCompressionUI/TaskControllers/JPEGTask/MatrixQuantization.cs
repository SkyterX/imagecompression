using System;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	[Serializable]
	public class MatrixQuantization : IQuantization
	{
		protected Matrix<double> QuantizationMatrix;

		protected MatrixQuantization()
		{
		}

		public MatrixQuantization(Matrix<double> quantizationMatrix)
		{
			QuantizationMatrix = quantizationMatrix;
		}

		public virtual int MatrixSize { get; set; }

		public void Apply(Matrix<double> matrix)
		{
			matrix.PointwiseDivide(QuantizationMatrix, matrix);
			matrix.Map(Math.Round, matrix);
		}

		public void Invert(Matrix<double> matrix)
		{
			matrix.PointwiseMultiply(QuantizationMatrix, matrix);
		}
	}

	[Serializable]
	public class FormulaMatrixQuantization : MatrixQuantization
	{
		private readonly double alpha;
		private readonly double gamma;
		private int matrixSize = 8;

		public FormulaMatrixQuantization(double alpha, double gamma)
		{
			this.alpha = alpha;
			this.gamma = gamma;
			QuantizationMatrix = GenerateMatrix(alpha, gamma, matrixSize);
		}

		private static Matrix<double> GenerateMatrix(double alpha, double gamma, int matrixSize)
		{
			return Matrix<double>.Build.Dense(matrixSize, matrixSize,
				(x, y) => alpha*(1 + gamma*(x + y + 2)));
		}

		public override int MatrixSize
		{
			get { return matrixSize; }
			set
			{
				matrixSize = value;
				QuantizationMatrix = GenerateMatrix(alpha, gamma, matrixSize);
			}
		}
	}
}