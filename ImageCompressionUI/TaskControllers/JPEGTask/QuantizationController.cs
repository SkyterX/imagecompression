using System;
using System.Windows.Forms;

namespace ImageCompressionUI.TaskControllers.JPEGTask
{
	public class QuantizationController : ITaskController
	{
		private ComboBox quantizationBox;
		private NumericUpDown paramAValue;
		private NumericUpDown paramBValue;
		private NumericUpDown paramCValue;
		private NumericUpDown paramDValue;
		private Label paramALabel;
		private Label paramBLabel;
		private Label paramCLabel;
		private Label paramDLabel;

		private static readonly IQuantizationMode[] quantizationModes =
		{
			new NoQuantizationMode(),
			new TopNQuantizationMode(),
			new JPEGQuantizationMode(),
			new FormulaMatrixQuantizationMode()
		};

		private IQuantizationMode QuantizationMode
		{
			get { return quantizationModes[quantizationBox.SelectedIndex]; }
		}

		public IQuantization[] Quantizations
		{
			get
			{
				return QuantizationMode.GetQuantizations(
					(double) paramAValue.Value,
					(double) paramBValue.Value,
					(double) paramCValue.Value,
					(double) paramDValue.Value);
			}
		}

		public void BindEvents(MainForm mainForm)
		{
			quantizationBox = mainForm.JPEGQuantizationBox;
			quantizationBox.SelectedIndexChanged += SelectedQuantizationModeChanged;
			paramAValue = mainForm.JPEGQuantizationParamAValue;
			paramALabel = mainForm.JPEGQuantizationParamALabel;
			paramBValue = mainForm.JPEGQuantizationParamBValue;
			paramBLabel = mainForm.JPEGQuantizationParamBLabel;
			paramCValue = mainForm.JPEGQuantizationParamCValue;
			paramCLabel = mainForm.JPEGQuantizationParamCLabel;	
			paramDValue = mainForm.JPEGQuantizationParamDValue;
			paramDLabel = mainForm.JPEGQuantizationParamDLabel;
		}

		public void Initialize()
		{
			quantizationBox.Items.Clear();
			foreach (var quantizationMode in quantizationModes)
			{
				quantizationBox.Items.Add(quantizationMode.DisplayName);
			}
			quantizationBox.SelectedIndex = 0;
		}

		public void UnbindEvents(MainForm mainForm)
		{
			quantizationBox.SelectedIndexChanged -= SelectedQuantizationModeChanged;
		}

		private void SelectedQuantizationModeChanged(object sender, EventArgs eventArgs)
		{
			var paramValues = new[] {paramAValue, paramBValue, paramCValue, paramDValue};
			var paramLabels = new[] {paramALabel, paramBLabel, paramCLabel, paramDLabel};
			for (int i = 0; i < paramValues.Length; i++)
			{
				paramValues[i].Visible = false;
				paramLabels[i].Visible = false;
			}
			var paramNames = QuantizationMode.ParamNames;
			for (var i = 0; i < Math.Min(paramValues.Length, paramNames.Length); i++)
			{
				var visible = paramNames[i] != null;
				paramValues[i].Visible = visible;
				paramLabels[i].Visible = visible;
				paramLabels[i].Text = paramNames[i] + " : ";
			}
		}
	}
}