namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public interface IWavelet
	{
		string DisplayName { get; }

		WaveletTransform Transform(CycledArray array);
		CycledArray Inverse(WaveletTransform transform);
	}

	public struct WaveletTransform
	{
		public readonly int Length;
		public readonly CycledArray Low;
		public readonly CycledArray High;

		public WaveletTransform(CycledArray high, CycledArray low, int length)
		{
			Low = low;
			Length = length;
			High = high;
		}
	}
}