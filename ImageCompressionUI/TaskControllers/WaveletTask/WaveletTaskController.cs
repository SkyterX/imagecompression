﻿using System;
using System.Windows.Forms;
using ImageCompressionUI.ImageProcessing;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public class WaveletTaskController : ConvertController
	{
		private readonly WaveletTypeController waveletType;
		private NumericUpDown waveletDepthValue;
		private NumericUpDown waveletThresholdValue;
		private bool initialized;
		private Label waveletCompressRatioValue;
		private Label waveletCompressedSizeValue;

		public WaveletTaskController()
		{
			initialized = false;
			waveletType = new WaveletTypeController();
			waveletType.Updated += Update;
		}
		public int WaveletDepth
		{
			get { return (int)waveletDepthValue.Value; }
		}

		public double WaveletThreshold
		{
			get { return (double) waveletThresholdValue.Value; }
		}

		protected override void BindInternalEvents(MainForm mainForm)
		{
			waveletType.BindEvents(mainForm);

			waveletDepthValue = mainForm.WaveletDepthValue;
			waveletThresholdValue = mainForm.WaveletThresholdValue;
			waveletCompressRatioValue = mainForm.WaveletCompressRatioValue;
			waveletCompressedSizeValue = mainForm.WaveletCompressedSizeValue;
			waveletDepthValue.ValueChanged += Update;
			waveletThresholdValue.ValueChanged += Update;
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			initialized = false;

			waveletType.UnbindEvents(mainForm);

			waveletDepthValue.ValueChanged -= Update;
			waveletThresholdValue.ValueChanged -= Update;
		}

		protected override void InitializeInternal()
		{
			waveletType.Initialize();

			initialized = true;
			UpdateConvertedImage();
		}

		private void Update(object sender, EventArgs eventArgs)
		{
			UpdateConvertedImage();
		}

		private void UpdateConvertedImage()
		{
			if (!initialized || State.LeftImage == null) return;

			var type = waveletType.Wavelet;
			var depth = WaveletDepth;
			var encoded = Wavelet.Encode(State.LeftImage, type, depth, WaveletThreshold);

			var baseSize = State.LeftImage.GetSize();
			var encodedSize = encoded.GetSize();
			var ratio = ((decimal) baseSize)/encodedSize;
			waveletCompressRatioValue.Text = ratio.ToString("0.000");
			waveletCompressedSizeValue.Text = Formatter.FormatMemory(encodedSize);

			var decoded = Wavelet.Decode(encoded, type, depth);
			State.RightImage = decoded;
			State.UpdatePSNR(State.LeftImage, State.RightImage);
			State.SetRightBitmap(State.RightImage);
		}
	}
}