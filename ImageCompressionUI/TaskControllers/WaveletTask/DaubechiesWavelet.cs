namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public class DaubechiesWavelet : AbstractWavelet
	{
		public static readonly DaubechiesWavelet D4 =
			new DaubechiesWavelet(new[] {0.6830127, 1.1830127, 0.3169873, -0.1830127});

		public static readonly DaubechiesWavelet D6 =
			new DaubechiesWavelet(new[] {0.47046721, 1.14111692, 0.650365, -0.19093442, -0.12083221, 0.0498175});


		public static readonly DaubechiesWavelet D8 =
			new DaubechiesWavelet(new[]
			{0.32580343, 1.01094572, 0.8922014, -0.03957503, -0.26450717, 0.0436163, 0.0465036, -0.01498699});

		private DaubechiesWavelet(double[] p) : base(p, p.Length - 2)
		{
		}

		public override string DisplayName
		{
			get { return "Daubechies " + P.Length; }
		}
	}
}