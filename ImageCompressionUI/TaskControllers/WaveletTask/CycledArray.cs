﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public abstract class CycledArray : IEnumerable<double>
	{
		public static readonly CycledArray Empty = new SimpleCycledArray(new double[0]);

		public double this[int idx]
		{
			get { return Get(idx); }
			set { Set(idx, value); }
		}

		public double Get(int idx)
		{
			return GetInternal(Mod(idx, Length));
		}

		public void Set(int idx, double val)
		{
			SetInternal(Mod(idx, Length), val);
		}

		private static int Mod(int idx, int length)
		{
			return (idx%length + length)%length;
		}

		public IEnumerator<double> GetEnumerator()
		{
			return new Enumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public override string ToString()
		{
			return ToString("0.000");
		}

		public string ToString(string format)
		{
			var sb = new StringBuilder();
			sb.Append("[");
			for (var i = 0; i < Length; i++)
			{
				sb.Append(GetInternal(i).ToString(format, CultureInfo.InvariantCulture));
				if(i + 1 != Length)
				sb.Append(", ");
			}
			sb.Append("]");
			return sb.ToString();
		}

		protected abstract void SetInternal(int idx, double val);
		protected abstract double GetInternal(int idx);
		public abstract int Length { get; }

		private struct Enumerator : IEnumerator<double>
		{
			private readonly CycledArray array;
			private int idx;

			public Enumerator(CycledArray array)
			{
				this.array = array;
				idx = -1;
			}

			public void Dispose()
			{
			}

			public bool MoveNext()
			{
				idx++;
				return idx < array.Length;
			}

			public void Reset()
			{
				idx = -1;
			}

			public double Current
			{
				get { return array[idx]; }
			}

			object IEnumerator.Current
			{
				get { return array[idx]; }
			}
		}
	}


	public static class CycledArrayExtension
	{
		public static CycledArray Cycle(this double[] array)
		{
			return new SimpleCycledArray(array);
		}
	}

	public class SimpleCycledArray : CycledArray
	{
		private readonly double[] array;

		public SimpleCycledArray(double[] array)
		{
			this.array = array;
		}

		protected override void SetInternal(int idx, double val)
		{
			array[idx] = val;
		}

		protected override double GetInternal(int idx)
		{
			return array[idx];
		}

		public override int Length
		{
			get { return array.Length; }
		}
	}

	public class MatrixRow : CycledArray
	{
		private readonly Matrix<double> matrix;
		private readonly int rowIndex;
		private readonly int offs;
		private readonly int length;

		public MatrixRow(Matrix<double> matrix, int rowIndex, int offs = 0, int length = -1)
		{
			this.matrix = matrix;
			this.rowIndex = rowIndex;
			this.offs = offs;
			this.length = length == -1 ? matrix.ColumnCount : length;
		}

		protected override double GetInternal(int idx)
		{
			return matrix.At(rowIndex, offs + idx);
		}

		protected override void SetInternal(int idx, double val)
		{
			matrix.At(rowIndex, offs + idx, val);
		}

		public override int Length
		{
			get { return length; }
		}
	}

	public class MatrixColumn : CycledArray
	{
		private readonly Matrix<double> matrix;
		private readonly int columnIndex;
		private readonly int offs;
		private readonly int length;

		public MatrixColumn(Matrix<double> matrix, int columnIndex, int offs = 0, int length = -1)
		{
			this.matrix = matrix;
			this.columnIndex = columnIndex;
			this.offs = offs;
			this.length = length == -1 ? matrix.ColumnCount : length;
		}

		protected override double GetInternal(int idx)
		{
			return matrix.At(offs + idx, columnIndex);
		}

		protected override void SetInternal(int idx, double val)
		{
			matrix.At(offs + idx, columnIndex, val);
		}
		
		public override int Length
		{
			get { return length; }
		}
	}
}