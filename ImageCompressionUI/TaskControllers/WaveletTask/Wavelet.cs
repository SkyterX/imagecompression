﻿using System;
using System.Threading.Tasks;
using ImageCompressionUI.ImageProcessing;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public static class Wavelet
	{
		public static Matrix<double>[] Encode(RgbImage image, IWavelet wavelet, int depth, double waveletThreshold)
		{
			var matrices = MatrixConverter.YCbCr.Convert(image);
			depth = Math.Min(depth, (int) Math.Log(matrices[0].RowCount, 2));
			depth = Math.Min(depth, (int) Math.Log(matrices[0].ColumnCount, 2));

			var width = image.Width;
			var height = image.Height;
			for (var depthIdx = 0; depthIdx < depth; depthIdx++)
			{
				for (var matrixIdx = 0; matrixIdx < matrices.Length; matrixIdx++)
				{
					var matrix = matrices[matrixIdx];

//					for (var rowIdx = 0; rowIdx < width; rowIdx++)
					{
						Parallel.For(0, width, rowIdx =>
							ApplyTransform(wavelet, new MatrixRow(matrix, rowIdx, 0, height)));
					}

//					for (var columnIdx = 0; columnIdx < width; columnIdx++)
					{
						Parallel.For(0, height, columnIdx =>
							ApplyTransform(wavelet, new MatrixColumn(matrix, columnIdx, 0, width)));
					}
				}
				width /= 2;
				height /= 2;
			}
			foreach (var matrix in matrices)
			{
				matrix.MapInplace(x => Math.Abs(x) < waveletThreshold ? 0 : (short) Math.Round(x));
			}

			return matrices;
		}

		private static void ApplyTransform(IWavelet wavelet, CycledArray array)
		{
			var transform = wavelet.Transform(array);
			var idx = 0;
			foreach (var val in transform.High)
			{
				array[idx++] = val/2;
			}
			foreach (var val in transform.Low)
			{
				array[idx++] = val/2;
			}
		}

		public static RgbImage Decode(Matrix<double>[] matrices, IWavelet wavelet, int depth)
		{
			depth = Math.Min(depth, (int) Math.Log(matrices[0].RowCount, 2));
			depth = Math.Min(depth, (int) Math.Log(matrices[0].ColumnCount, 2));

			var width = (int) (matrices[0].RowCount/Math.Pow(2, depth - 1));
			var height = (int) (matrices[0].ColumnCount/Math.Pow(2, depth - 1));
//			var width = matrices[0].RowCount;
//			var height = matrices[0].ColumnCount;
			for (var depthIdx = 0; depthIdx < depth; depthIdx++)
			{
				for (var matrixIdx = 0; matrixIdx < matrices.Length; matrixIdx++)
				{
					var matrix = matrices[matrixIdx];

//					for (var columnIdx = 0; columnIdx < width; columnIdx++)
					{
						Parallel.For(0, height, columnIdx =>
							ApplyInverse(wavelet, new MatrixColumn(matrix, columnIdx, 0, width/2),
								new MatrixColumn(matrix, columnIdx, width/2, width/2)));
					}

//					for (var rowIdx = 0; rowIdx < width; rowIdx++)
					{
						Parallel.For(0, width, rowIdx =>
							ApplyInverse(wavelet, new MatrixRow(matrix, rowIdx, 0, height/2),
								new MatrixRow(matrix, rowIdx, height/2, height/2)));
					}
				}
				width *= 2;
				height *= 2;
			}

			return MatrixConverter.YCbCr.Convert(matrices);
		}

		private static void ApplyInverse(IWavelet wavelet, CycledArray high, CycledArray low)
		{
			var transform = wavelet.Inverse(new WaveletTransform(high, low, high.Length + low.Length));
			var idx = 0;
			for (var i = 0; i < high.Length; i++)
			{
				high[i] = 2*transform[idx++];
			}
			for (var i = 0; i < low.Length; i++)
			{
				low[i] = 2*transform[idx++];
			}
		}
	}
}