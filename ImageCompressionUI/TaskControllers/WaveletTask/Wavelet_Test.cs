using System;
using System.Linq;
using ImageCompressionUI.Util;
using MathNet.Numerics.LinearAlgebra.Double;
using NUnit.Framework;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	[TestFixture]
	public class Wavelet_Test
	{
		[Test]
		public void ShowCoeffs()
		{
//			var wv = DaubechiesWavelet.D4;
			var wv = HaarWavelet.Instance;

			Console.Out.WriteLine("P    = {0}", wv.P.Cycle());
			Console.Out.WriteLine("Q    = {0}", wv.Q.Cycle());
			Console.Out.WriteLine("PInv = {0}", wv.PInv.Cycle());
			Console.Out.WriteLine("QInv = {0}", wv.QInv.Cycle());
		}

		private static readonly TestCaseData[] tcs =
		{
			new TestCaseData(new double[] {1, 2, 3, 4}).SetName("1 2 3 4"),
			new TestCaseData(new double[] {1, 2, 3, 4, 5, 6}).SetName("1 2 3 4 5 6"),
			new TestCaseData(new double[] {1, 2, 3, 4, 5, 6, 7, 8}).SetName("1 ... 8"),
			new TestCaseData(new double[] {1, 1, 1, 1}).SetName("1 1 1 1"),
			new TestCaseData(new double[] {1, 2}).SetName("1 2"),
			new TestCaseData(new double[] {1}).SetName("1"),
			new TestCaseData(new double[64].Fill(i => 1.0*i)).SetName("Linear 64"),
			new TestCaseData(new double[128].Fill(i => 0.001*i*i)).SetName("Quadratic 128"),
			new TestCaseData(new double[256].Fill(i => 0.00001*i*i*i)).SetName("Cubic 256"),
			new TestCaseData(Vector.Build.Random(512).ToArray()).SetName("Random 512")
		};

		[Test]
		[TestCaseSource("tcs")]
		public void TestHaar(double[] array)
		{
			var wavelet = HaarWavelet.Instance;
			TestArray(array, wavelet);
		}

		[Test]
		[TestCaseSource("tcs")]
		public void TestDaubechies4(double[] array)
		{
			var wavelet = DaubechiesWavelet.D4;
			TestArray(array, wavelet);
		}

		[Test]
		[TestCaseSource("tcs")]
		public void TestDaubechies6(double[] array)
		{
			var wavelet = DaubechiesWavelet.D6;
			TestArray(array, wavelet);
		}

		[Test]
		[TestCaseSource("tcs")]
		public void TestDaubechies8(double[] array)
		{
			var wavelet = DaubechiesWavelet.D8;
			TestArray(array, wavelet);
		}

		private static void TestArray(double[] array, IWavelet wavelet)
		{
			var x = array.Cycle();
			Console.Out.WriteLine("X    = {0}", x);
			var transform = wavelet.Transform(x);
			Console.Out.WriteLine("Low  = {0}", transform.Low);
			Console.Out.WriteLine("High = {0}", transform.High);
			var y = wavelet.Inverse(transform);
			Console.Out.WriteLine("Y    = {0}", y);
			var xArr = x.Select(z => Math.Round(z, 3)).ToArray();
			var yArr = y.Select(z => Math.Round(z, 3)).ToArray();
			Assert.That(xArr, Is.EqualTo(yArr));
		}
	}
}