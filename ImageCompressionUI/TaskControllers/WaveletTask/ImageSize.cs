﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ICSharpCode.SharpZipLib.GZip;
using ImageCompressionUI.ImageProcessing;
using MathNet.Numerics.LinearAlgebra;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public static class ImageSize
	{
		public static long GetSize(this RgbImage image)
		{
			var array = new byte[image.Width*image.Height*3];
			image.FillArray(array, 0, clr => clr.R);
			image.FillArray(array, 1, clr => clr.G);
			image.FillArray(array, 2, clr => clr.B);
			var formatter = new BinaryFormatter();
			var memoryStream = new MemoryStream(array.Length);
			using (var zipStream = new GZipOutputStream(memoryStream))
			{
				formatter.Serialize(zipStream, array);
			}
			return memoryStream.ToArray().Length;
		}

		private static void FillArray(this RgbImage image, byte[] array, int offs, Func<RgbColor, byte> getComponent)
		{
			var idx = image.Width*image.Height*offs;
			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					array[idx++] = getComponent(image[x, y]);
				}
			}
		}

		public static long GetSize(this Matrix<double>[] matrices)
		{
			var array = new short[matrices[0].RowCount*matrices[0].ColumnCount*3];
			var idx = 0;
			foreach (var matrix in matrices)
			{
				foreach (var val in matrix.Enumerate())
				{
					var shortVal = (short) val;
					array[idx++] = shortVal;
				}
			}
			var formatter = new BinaryFormatter();
			var memoryStream = new MemoryStream(array.Length);
			using (var zipStream = new GZipOutputStream(memoryStream))
			{
				formatter.Serialize(zipStream, array);
			}
			return memoryStream.ToArray().Length;
		}
	}
}