using System;
using System.Windows.Forms;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public class WaveletTypeController : ITaskController
	{
		private ComboBox waveletTypeBox;
		public event EventHandler Updated = delegate { };

		private static readonly IWavelet[] wavelet =
		{
			HaarWavelet.Instance,
			DaubechiesWavelet.D4,
			DaubechiesWavelet.D6,
			DaubechiesWavelet.D8
		};

		public IWavelet Wavelet
		{
			get { return wavelet[waveletTypeBox.SelectedIndex]; }
		}

		public void BindEvents(MainForm mainForm)
		{
			waveletTypeBox = mainForm.WaveletTypeBox;
			waveletTypeBox.SelectedIndexChanged += SelectedIndexChanged;
		}

		public void Initialize()
		{
			waveletTypeBox.Items.Clear();
			foreach (var waveletType in wavelet)
			{
				waveletTypeBox.Items.Add(waveletType.DisplayName);
			}
			waveletTypeBox.SelectedIndex = 0;
		}

		public void UnbindEvents(MainForm mainForm)
		{
			waveletTypeBox.SelectedIndexChanged -= SelectedIndexChanged;
		}

		private void SelectedIndexChanged(object sender, EventArgs eventArgs)
		{
			Updated.Raise(this);
		}
	}
}