using System;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	[Serializable]
	public abstract class AbstractWavelet : IWavelet
	{
		private readonly int shift;
		public readonly double[] P;
		public readonly double[] Q;
		public readonly double[] PInv;
		public readonly double[] QInv;

		protected AbstractWavelet(double[] p, double[] q, double[] pInv, double[] qInv, int shift)
		{
			this.shift = shift;
			P = Normalize(p);
			Q = Normalize(q);
			PInv = Normalize(pInv);
			QInv = Normalize(qInv);
		}

		protected AbstractWavelet(double[] p, double[] q, int shift)
			: this(p, q, p, q, shift)
		{
		}

		protected AbstractWavelet(double[] p, int shift)
			: this(p, GetQ(p), shift)
		{
		}

		private static double[] GetQ(double[] array)
		{
			var mult = 1;
			var result = new double[array.Length];
			for (var i = 0; i < array.Length; i++)
			{
				result[i] = array[i]*mult;
				mult *= -1;
			}
			Array.Reverse(result);
			return result;
		}

		private static double[] Normalize(double[] array)
		{
			var sum = 0.0;
			var sum2 = 0.0;
			for (var i = 0; i < array.Length; i += 2)
			{
				sum += array[i];
				if (i + 1 < array.Length)
					sum2 += array[i + 1];
			}
			sum = Math.Abs(sum);
			sum2 = Math.Abs(sum2);
			var result = new double[array.Length];
			for (var i = 0; i < array.Length; i += 2)
			{
				result[i] = array[i]/sum;
				if (i + 1 < array.Length)
					result[i + 1] = array[i + 1]/sum2;
			}
			return result;
		}


		public WaveletTransform Transform(CycledArray array)
		{
			if(array.Length == 1)
				return new WaveletTransform(array, CycledArray.Empty, 1);

			var n = (array.Length + 1)/2;
			var low = new double[n];
			var high = new double[n];

			for (var m = 0; m < n; m ++)
			{
				for (var l = 0; l < P.Length; l++)
				{
					var x = array[2*m + l];
					high[m] += x*P[l];
					low[m] += x*Q[l];
				}
			}

			return new WaveletTransform(high.Cycle(), low.Cycle(), array.Length);
		}

		public CycledArray Inverse(WaveletTransform transform)
		{
			if (transform.Length == 1)
				return transform.High;

			var low = transform.Low;
			var high = transform.High;

			var result = new double[transform.Length].Cycle();

			for (var m = 0; m < result.Length; m ++)
			{
				var k = 0;
				for (var l = PInv.Length - 1 - (m + 1)%2; l >= 0; l -= 2, k++)
				{
					result[m + shift] += high[m/2 + k]*PInv[l] + low[m/2 + k]*QInv[l];
				}
				result[m + shift] /= 2;
			}
			return result;
		}

		public abstract string DisplayName { get; }
	}
}