namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public class HaarWavelet : AbstractWavelet
	{
		public static readonly HaarWavelet Instance = new HaarWavelet();

		private HaarWavelet()
			: base(new[] {1d, 1d}, 0)
		{
		}

		public override string DisplayName
		{
			get { return "Haar"; }
		}
	}
}