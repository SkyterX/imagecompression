﻿using System;
using System.Windows.Forms;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.TaskControllers.WaveletTask
{
	public class WaveletDepthController : ITaskController
	{
		private NumericUpDown waveletDepthValue;

		public event EventHandler Updated = delegate { };

		public int WaveletDepth
		{
			get { return (int) waveletDepthValue.Value; }
		}

		public void BindEvents(MainForm mainForm)
		{
			waveletDepthValue = mainForm.WaveletDepthValue;
			waveletDepthValue.ValueChanged += DepthValueChanged;
		}

		public void Initialize()
		{
			waveletDepthValue.Value = 1;
		}

		public void UnbindEvents(MainForm mainForm)
		{
			waveletDepthValue.ValueChanged -= DepthValueChanged;
		}

		private void DepthValueChanged(object sender, EventArgs eventArgs)
		{
			Updated.Raise(this);
		}
	}
}