using System;
using System.Windows.Forms;
using ImageCompressionUI.ImageProcessing;

namespace ImageCompressionUI.TaskControllers
{
	public class LBGTaskController : ConvertController
	{
		private Label countLabel;
		private LBGFilter lbgFilter;

		protected override void BindInternalEvents(MainForm mainForm)
		{
			mainForm.LBGCountTrackBar.Scroll += LBGCountTrackBarOnScroll;
			mainForm.LBGApplyButton.Click += LbgApplyButtonOnClick;

			countLabel = mainForm.LBGCountValueLabel;
			countLabel.Text = mainForm.LBGCountTrackBar.Value.ToString();
			lbgFilter = new LBGFilter(mainForm.LBGCountTrackBar.Value);
		}

		protected override void InitializeInternal()
		{
			State.SetRightBitmap(State.LeftImage);
		}

		protected override void UnbindInternalEvents(MainForm mainForm)
		{
			mainForm.LBGCountTrackBar.Scroll -= LBGCountTrackBarOnScroll;
			mainForm.LBGApplyButton.Click -= LbgApplyButtonOnClick;
		}

		private void LbgApplyButtonOnClick(object sender, EventArgs eventArgs)
		{
			var button = (Button) sender;
			button.Enabled = false;
			UpdateConvertedImage();
			button.Enabled = true;
		}

		private void LBGCountTrackBarOnScroll(object sender, EventArgs eventArgs)
		{
			var trackBar = (TrackBar) sender;
			countLabel.Text = trackBar.Value.ToString();
			lbgFilter.ColorsCount = trackBar.Value;
		}

		private void UpdateConvertedImage()
		{
			if (State.LeftImage == null) return;
			State.RightImage = State.LeftImage
				.Clone()
				.Apply(lbgFilter);
			State.UpdatePSNR(State.LeftImage, State.RightImage);
			State.SetRightBitmap(State.RightImage);
		}
	}
}