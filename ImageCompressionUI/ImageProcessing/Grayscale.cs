﻿namespace ImageCompressionUI.ImageProcessing
{
	public class Grayscale : ConvertColor
	{
		public static readonly Grayscale EqualWeight = new Grayscale(1.0/3, 1.0/3, 1.0/3);
		public static readonly Grayscale CCIR = new Grayscale(0.299, 0.587, 0.114);

		private readonly double rWeight;
		private readonly double gWeight;
		private readonly double bWeight;

		private Grayscale(double rWeight, double gWeight, double bWeight)
		{
			this.rWeight = rWeight;
			this.gWeight = gWeight;
			this.bWeight = bWeight;
		}

		protected override RgbColor Convert(RgbColor baseColor)
		{
			var R = baseColor.R;
			var G = baseColor.G;
			var B = baseColor.B;

			var Y = ToByte(R*rWeight + G*gWeight + B*bWeight);

			return new RgbColor(Y, Y, Y);
		}
	}
}