﻿using System.Drawing;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.ImageProcessing
{
	public class RgbImage
	{
		private readonly RgbColor[,] image;

		public int Width { get; private set; }
		public int Height { get; private set; }

		public RgbImage(int width, int height)
		{
			Width = width;
			Height = height;
			image = new RgbColor[Width, Height];
		}

		public RgbImage(Bitmap bitmap)
			: this(bitmap.Width, bitmap.Height)
		{
			for (var x = 0; x < Width; x++)
			{
				for (var y = 0; y < Height; y++)
				{
					var col = bitmap.GetPixel(x, y);
					image[x, y] = new RgbColor(col.R, col.G, col.B);
				}
			}
		}

		private RgbImage(int width, int height, RgbColor[,] image)
			: this(width, height)
		{
			for (var x = 0; x < Width; x++)
			{
				for (var y = 0; y < Height; y++)
				{
					this.image[x, y] = image[x, y];
				}
			}
		}

		public RgbImage Clone()
		{
			return new RgbImage(Width, Height, image);
		}

		public void ToBitmap(Bitmap bitmap)
		{
			bitmap.Fill(Color.White);
			for (var x = 0; x < Width; x++)
			{
				for (var y = 0; y < Height; y++)
				{
					var rCol = image[x, y];
					var color = Color.FromArgb(rCol.R, rCol.G, rCol.B);
					bitmap.SetPixel(x, y, color);
				}
			}
		}

		public RgbColor this[int x, int y]
		{
			get { return image[x, y]; }
			set { image[x, y] = value; }
		}
	}
}