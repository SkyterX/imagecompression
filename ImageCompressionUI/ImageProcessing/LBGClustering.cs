﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.ImageProcessing
{
	public class LBGClustering
	{
		private bool ShowIterations = false;
		private const double moveEps = 1e-3;
		private const int maxOptimizeIterations = 100;

		private readonly double[][] vectors;
		private readonly double[] weights;
		private readonly int nVars;
		private readonly int nCentroids;

		private readonly int[] vectorId;
		private readonly List<double[]> centroids;
		private readonly StringBuilder log;

		public LBGClustering(double[][] vectors, int nCentroids, double[] weights = null)
		{
			this.vectors = vectors;
			nVars = vectors[0].Length;
			this.nCentroids = nCentroids;
			this.weights = weights ?? new double[nVars].Fill(1);
			vectorId = new int[vectors.Length];
			centroids = new List<double[]>(nCentroids);
			log = new StringBuilder();
		}

		public List<double[]> CalculateCentroids()
		{
			Initialize();
			while (centroids.Count < nCentroids)
			{
				Split();
				Optimize();
			}
			if(ShowIterations)
				MessageBox.Show(log.ToString(), "LBG iterations");
			return centroids;
		}

		private void Initialize()
		{
			centroids.Add(new double[nVars]);
			UpdateCentroids();
		}

		private void UpdateCentroids()
		{
			var count = new int[centroids.Count];
			centroids.ConvertThis(centroid => new double[centroid.Length]);
			for (var i = 0; i < vectors.Length; i++)
			{
				count[vectorId[i]]++;
				var c = centroids[vectorId[i]];
				for (var j = 0; j < nVars; j++)
				{
					c[j] += vectors[i][j];
				}
			}
			for (var i = 0; i < centroids.Count; i++)
			{
				if (count[i] == 0) continue;
				for (var j = 0; j < nVars; j++)
				{
					centroids[i][j] /= count[i];
				}
			}
		}

		private void Split()
		{
			var newCentroids = Math.Min(nCentroids - centroids.Count, centroids.Count);
			for (var i = 0; i < newCentroids; i++)
			{
				var cP = MoveCentroid(centroids[i], 1);
				var cN = MoveCentroid(centroids[i], -1);
				centroids[i] = cP;
				centroids.Add(cN);
			}
		}

		private double[] MoveCentroid(double[] centroid, int direction)
		{
			return Array.ConvertAll(centroid, x => x*(1 + moveEps*direction));
		}

		private void Optimize()
		{
			UpdateVectorIds();
			var changed = true;
			var iterationsLeft = maxOptimizeIterations;
			while (iterationsLeft-- > 0 && changed)
			{
				UpdateCentroids();
				changed = UpdateVectorIds();
			}
			log.AppendFormat("Centroids : {0,5}, Iterations : {1, -1}",
				centroids.Count, maxOptimizeIterations - iterationsLeft - 1)
				.AppendLine();
		}

		private bool UpdateVectorIds()
		{
			var changed = false;
			for (var i = 0; i < vectors.Length; i++)
			{
				var newId = centroids.IndexOfMin(centroid => Distance(vectors[i], centroid));
				changed = vectorId[i] != newId;
				vectorId[i] = newId;
			}
			return changed;
		}

		public double Distance(double[] vector, double[] centroid)
		{
			var distance = 0.0;
			for (var i = 0; i < nVars; i++)
			{
				var x = centroid[i] - vector[i];
				distance += x*x*weights[i];
			}
			return distance;
		}
	}
}