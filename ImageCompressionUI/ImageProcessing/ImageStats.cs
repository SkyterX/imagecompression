﻿using System;

namespace ImageCompressionUI.ImageProcessing
{
	public static class ImageStats
	{
		public static double PSNR(RgbImage a, RgbImage b)
		{
			double MSE = 0;
			for (var x = 0; x < a.Width; x++)
			{
				for (var y = 0; y < a.Height; y++)
				{
					var aC = a[x, y];
					var bC = b[x, y];
					MSE += Sqr(aC.R - bC.R) + Sqr(aC.G - bC.G) + Sqr(aC.B - bC.B);
				}
			}
			MSE /= (a.Width*a.Height*3.0);
			if (MSE == 0)
				return double.PositiveInfinity;
			var PSNR = 10*Math.Log10(255*255/MSE);
			return PSNR;
		}

		private static double Sqr(double x)
		{
			return x*x;
		}
	}
}