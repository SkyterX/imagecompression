using System;
using System.Collections.Generic;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.ImageProcessing
{
	public class LBGPFilter : IFilter
	{
		public int ColorsCount { get; set; }
		public double Color2PositionRelation { get; set; }

		public LBGPFilter(int colorsCount = 1, double color2PositionRelation = 0)
		{
			ColorsCount = colorsCount;
			Color2PositionRelation = color2PositionRelation;
		}

		public void Apply(RgbImage image)
		{
			var weights = CalculateWeights();
			new State(image, ColorsCount, weights).Apply(image);
		}

		private double[] CalculateWeights()
		{
			var weights = new double[5].Fill(1);
			if (Color2PositionRelation < 0)
			{
				weights[0] -= Color2PositionRelation;
				weights[1] -= Color2PositionRelation;
				weights[2] -= Color2PositionRelation;
			}
			else
			{
				weights[3] += Color2PositionRelation;
				weights[4] += Color2PositionRelation;
			}
			return weights;
		}

		private class State : IFilter
		{
			private readonly RgbColor[] centroidColors;
			private readonly List<double[]> centroids;
			private readonly double[][] vectors;
			private readonly LBGClustering clustering;

			public State(RgbImage image, int colorsCount, double[] weights)
			{
				vectors = new double[image.Width*image.Height][];
				var vIdx = 0;
				var width = (double) image.Width;
				var height = (double) image.Height;
				for (var x = 0; x < image.Width; x++)
				{
					for (var y = 0; y < image.Height; y++)
					{
						var color = image[x, y];
						var vector = new[] {color.R/256.0, color.G/256.0, color.B/256.0, x/width, y/height};
						vectors[vIdx++] = vector;
					}
				}
				clustering = new LBGClustering(vectors, colorsCount, weights);
				centroids = clustering.CalculateCentroids();
				centroidColors = new RgbColor[colorsCount];
				SetCentroidColors();
			}

			public void Apply(RgbImage image)
			{
				for (var x = 0; x < image.Width; x++)
				{
					for (var y = 0; y < image.Height; y++)
					{
						image[x, y] = Convert(x*image.Height + y);
					}
				}
			}

			private byte ToByte(double value)
			{
				var intVal = (int) Math.Round(value);
				if (intVal > 255) return 255;
				if (intVal < 0) return 0;
				return (byte) intVal;
			}

			private void SetCentroidColors()
			{
				for (var i = 0; i < centroids.Count; i++)
				{
					centroidColors[i] = new RgbColor(
						ToByte(centroids[i][0] * 256),
						ToByte(centroids[i][1] * 256),
						ToByte(centroids[i][2] * 256));
				}
			}

			private RgbColor Convert(int vectorId)
			{
				var centroidId = centroids.IndexOfMin(centroid => clustering.Distance(vectors[vectorId], centroid));
				return centroidColors[centroidId];
			}
		}
	}
}