﻿namespace ImageCompressionUI.ImageProcessing
{
	public class TruncateFilter : IFilter
	{
		private int rSize;
		private int gSize;
		private int bSize;

		public TruncateFilter(int rSize, int gSize, int bSize)
		{
			RSize = rSize;
			GSize = gSize;
			BSize = bSize;
		}

		public int RSize
		{
			get { return rSize; }
			set { rSize = Clamp(value); }
		}

		public int GSize
		{
			get { return gSize; }
			set { gSize = Clamp(value); }
		}

		public int BSize
		{
			get { return bSize; }
			set { bSize = Clamp(value); }
		}

		public void Apply(RgbImage image)
		{
			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					var baseColor = image[x, y];
					var convertedColor = TruncateColor(baseColor);
					image[x, y] = convertedColor;
				}
			}
		}

		private RgbColor TruncateColor(RgbColor baseColor)
		{
			var R = Truncate(baseColor.R, RSize);
			var G = Truncate(baseColor.G, GSize);
			var B = Truncate(baseColor.B, BSize);
			return new RgbColor(R, G, B);
		}

		private byte Truncate(byte x, int size)
		{
			var trunk = 8 - size;
			x >>= trunk;
			x <<= trunk;
			var mid = trunk == 0 ? 0 : (1 << (trunk - 1));
			return (byte) (x + mid);
		}

		private int Clamp(int value)
		{
			if (value < 0) return 0;
			if (value > 8) return 8;
			return value;
		}
	}
}