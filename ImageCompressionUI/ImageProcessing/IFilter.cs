﻿namespace ImageCompressionUI.ImageProcessing
{
	public interface IFilter
	{
		void Apply(RgbImage image);
	}

	public static class FilterExtensions
	{
		public static RgbImage Apply(this RgbImage image, IFilter filter)
		{
			filter.Apply(image);
			return image;
		}
	}
}