﻿using System.Collections.Generic;
using ImageCompressionUI.Util;

namespace ImageCompressionUI.ImageProcessing
{
	public class LBGFilter : IFilter
	{
		public int ColorsCount { get; set; }

		public LBGFilter(int colorsCount)
		{
			ColorsCount = colorsCount;
		}

		public void Apply(RgbImage image)
		{
			new State(image, ColorsCount).Apply(image);
		}

		private class State : ConvertColor
		{
			private readonly RgbColor[] centroidColors;
			private readonly List<double[]> centroids;

			public State(RgbImage image, int colorsCount)
			{
				var vectors = new double[image.Width*image.Height][];
				var vIdx = 0;
				for (var x = 0; x < image.Width; x++)
				{
					for (var y = 0; y < image.Height; y++)
					{
						var color = image[x, y];
						var vector = new double[] {color.R, color.G, color.B};
						vectors[vIdx++] = vector;
					}
				}
				centroids = new LBGClustering(vectors, colorsCount).CalculateCentroids();
				centroidColors = new RgbColor[colorsCount];
				SetCentroidColors();
			}

			private void SetCentroidColors()
			{
				for (var i = 0; i < centroids.Count; i++)
				{
					centroidColors[i] = new RgbColor(
						ToByte(centroids[i][0]),
						ToByte(centroids[i][1]),
						ToByte(centroids[i][2]));
				}
			}

			private double Distance(RgbColor color, double[] centroid)
			{
				return Sqr(centroid[0] - color.R) + Sqr(centroid[1] - color.G) + Sqr(centroid[2] - color.B);
			}

			private double Sqr(double x)
			{
				return x*x;
			}

			protected override RgbColor Convert(RgbColor baseColor)
			{
				var centroidId = centroids.IndexOfMin(centroid => Distance(baseColor, centroid));
				return centroidColors[centroidId];
			}
		}
	}
}