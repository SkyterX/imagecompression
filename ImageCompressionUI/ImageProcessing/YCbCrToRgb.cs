﻿namespace ImageCompressionUI.ImageProcessing
{
	public class YCbCrToRgb : ConvertColor
	{
		public static readonly YCbCrToRgb Instance = new YCbCrToRgb();

		private YCbCrToRgb()
		{
		}

		protected override RgbColor Convert(RgbColor baseColor)
		{
			var Y = baseColor.R;
			var Cb = baseColor.G - 128;
			var Cr = baseColor.B - 128;

			var R = ToByte(Y + 1.402*Cr);
			var G = ToByte(Y - 0.34414*Cb - 0.71414*Cr);
			var B = ToByte(Y + 1.772*Cb);
			return new RgbColor(R, G, B);
		}
	}
}