﻿using System;

namespace ImageCompressionUI.ImageProcessing
{
	public abstract class ConvertColor : IFilter
	{
		public void Apply(RgbImage image)
		{
			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					var baseColor = image[x, y];
					var convertedColor = Convert(baseColor);
					image[x, y] = convertedColor;
				}
			}
		}

		protected abstract RgbColor Convert(RgbColor baseColor);

		protected byte ToByte(double value)
		{
			var intVal = (int)Math.Round(value);
			if (intVal > 255) return 255;
			if (intVal < 0) return 0;
			return (byte)intVal;
		}
	}
}