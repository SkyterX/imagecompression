﻿namespace ImageCompressionUI.ImageProcessing
{
	public class RgbToYCbCr : ConvertColor
	{
		public static readonly RgbToYCbCr Instance = new RgbToYCbCr();

		private RgbToYCbCr()
		{
		}

		protected override RgbColor Convert(RgbColor baseColor)
		{
			var R = baseColor.R;
			var G = baseColor.G;
			var B = baseColor.B;

			var Y = ToByte(R*0.299 + G*0.587 + B*0.114);
			var Cb = ToByte(-R*0.168736 - G*0.331264 + B*0.5 + 128);
			var Cr = ToByte(R*0.5 - G*0.418688 - B*0.081313 + 128);
			return new RgbColor(Y, Cb, Cr);
		}
	}
}